#Import("*")

import os,sys,types
import string,struct
import shutil
#import pdb
import stat

import struct

import usbimages

import binascii 

def getFileCRC(_path): 
    try: 
        blocksize = 1024 * 64 
        f = open(_path,"rb") 
        str = f.read(blocksize) 
        crc = 0 
        while(len(str) != 0): 
            crc = binascii.crc32(str, crc)
            str = f.read(blocksize) 
        f.close() 
    except: 
        print 'get file crc error!' 
        return 0 
    return crc  
    

#inst_dir = Dir(env.builddir + "/install").abspath

#def i(file_name):
#    return os.path.join(inst_dir, file_name)

#files = []

#make dir
#if not os.path.exists(dst_path):
#   os.mkdir(dst_path)


# file chksum function 1byte sum 
def checksum_f(filename):
   file_chksum = 0L
   file_size=os.path.getsize(filename)
   
   print "%d" % file_size
   if file_size <= 0x1E00000:
      fp=file(filename,'r')
      content=fp.read(file_size)
      fp.close()
      
      for i in range(0,file_size):   
        file_chksum = file_chksum + ord(content[i])
        
   else:
      # file_chksum = getFileCRC(filename)  # use crc32 ,but no use
      fp=file(filename,'r')                 # use skip 4byte 
      content=fp.read(file_size)
      fp.close()
      
      for i in range(0,file_size,4):
        #if i % 4 == 0:
        file_chksum = file_chksum + ord(content[i])

   print "check sum compelet = %d!" %   file_chksum
   
   return file_chksum,file_size,(file_size + usbimages.sector_size) / usbimages.sector_size

# no use
"""
def checksum_f(filename,twice=0):
   file_chksum=0
   file_size=os.path.getsize(filename)
   if file_size > 31457280: #0x1E00000 ,30M
      if twice == 0:
        file_size = 31457280
      else:
        file_size = file_size - 31457280
   
   fp=file(filename,'r')
   if twice == 1:
     fp.seek(31457280)
   #else:
   #  fp.seek(0)

   print file_size
   content=fp.read(file_size)
   fp.close()
   for i in range(0,file_size):   #from 0 to file_size-1 !!!
     file_chksum = file_chksum + ord(content[i])
   return file_chksum
"""


####################
# start 
####################


my_images = []

# sort class usb image
for each in dir(usbimages):
    attr = getattr(usbimages, each)
    if type(attr) == types.ClassType and issubclass(attr, usbimages.usbImage) and not attr.virtual:
       #print attr.name
       my_images.append(attr)

options = [(op_image.order, op_image) for op_image in my_images]
options = sorted(options)



"""
struct image_info {        /* 32 bytes total*/
    char part_name[16+2];  /* +2 for 4byte aligned */
    uint8_t flags;
    uint8_t type;
    uint32_t offset;
    uint32_t size;         /* in bytes */
    uint32_t sectors;      /* in sectors */
    uint32_t checksum;     /* check sum */
};
struct install_info {
    //char device[24];            /* tmp images device name ,liunote use it ??? */
    uint32_t magic                /* 0x5F646E77 'dnw_' */
    uint32_t start_tmp_offset;    /* if usb bulk at kernel ,doesnt need this filed!!! ,in sectors */
    //int sect_size;              /* expected sector size in bytes. MUST BE POWER OF 2 , now is 512 bytes */
    uint32_t num_images;
    struct image_info *image_lst;
};
"""

# open download file for write
if os.path.isfile(usbimages.usb_download_file_name):
   os.remove(usbimages.usb_download_file_name)
       
out_f = file(usbimages.usb_download_file_name,'wb')

buf = struct.pack("I",usbimages.magic)
buf += struct.pack("I",usbimages.download_img_raw_start)

# process images nums
num_images = 0
for each in options:
  #print  each[1].name
  src_file = os.path.join(usbimages.images_path_offset[each[1].name][0],each[1].name)
  screen_info = src_file + " header processing... "
  if os.path.isfile(src_file):              # src img exists
     num_images +=1
     
assert num_images > 0

buf += struct.pack("I",num_images)


# process images header  (512byte)
for each in options:
  src_file = os.path.join(usbimages.images_path_offset[each[1].name][0],each[1].name)
  screen_info = src_file + " header processing... "
  if os.path.isfile(src_file):              # src img exists
    print screen_info
    #shutil.copy(src,dest)
    checksum,size,sectors = checksum_f(src_file);
    flags = 0
    if(hasattr(each[1],"flags")):
      flags = each[1].flags
    type = 0
    if(hasattr(each[1],"type")):
      type = each[1].type
    offset = 0  
    if(hasattr(each[1],"offset")):
      offset = each[1].offset

    partition = ""
    if(hasattr(each[1],"partition")):
      partition = each[1].partition
      
    action = 0;
    if(hasattr(each[1],"action")):
      action = each[1].action

    buf += struct.pack("16sBBIIIIH",partition,flags,type,offset,size,sectors,checksum,action);
    
assert len(buf) <= 512

buf += '\0' * (usbimages.sector_size - len(buf))

out_f.write(buf)


# process images write
for each in options:
  src_file = os.path.join(usbimages.images_path_offset[each[1].name][0],each[1].name)
  screen_info = src_file + " wirte processing... "
  if os.path.isfile(src_file):              # src img exists
     print screen_info
     file_size = os.path.getsize(src_file)
     in_f = file(src_file,'r')
     content = in_f.read(file_size)
     in_f.close()
     out_f.write(content)
     if len(content) % usbimages.sector_size != 0 :  # aliged to sector size (512)
        out_f.write('\0' * (usbimages.sector_size - len(content) % usbimages.sector_size))
     
     
out_f.close()  

######   
     
