# liu add usb download images class

############################################################################
#  custom setting !!!
############################################################################

scheme = "mbr"

start_lba = 2048

# setting disk 's sector size
sector_size = 512

# setting dnw download img file name
usb_download_file_name = "dnw.img"

# setting u-boot dnw raw data 's start sector ,see disk layout conf (must match it !!!)
download_img_raw_start = ((128 + 128) * 1024 * 1024) / sector_size   # in sectors (uboot down load to third_part!!!)

download_img_raw_start += start_lba

if(scheme == "mbr"):
   download_img_raw_start +=1
   
# 526337   
print "u-boot movi write to %d sector" %  download_img_raw_start



# setting bootstep form header or ender (samsung pk prima)
last_sect = 0                   # if pc100 get last_sect from u-boot, prima is 0

# setting fix sector for raw images !!!
u_boot_sectors         = 1024   # 512K
ramdisk_uboot_sectors  = 2048   # 1M
zImage_sectors         = 8192   # 4M
u_tools_sectors        = 8192   # 4M


# setting src img path
u_boot_path            = ''               # '' is current path !!!
ramdisk_uboot_path     = ''
zImage_path            = '' 
u_tools_path           = ''  
system_path            = ''
userdata_path          = '' 
otherdata_path         = ''

############################################################################

magic =  0x5F776E64  #0x5F646E77  # 'dnw_'


images_path_offset = \
    {'u-boot.bin':           (u_boot_path,       0 if last_sect == 0 else last_sect),    
     'zImage':               (zImage_path,       u_boot_sectors if last_sect == 0 else (last_sect - u_boot_sectors)),
     'ramdisk-uboot.img':    (ramdisk_uboot_path,(u_boot_sectors + zImage_sectors) if last_sect == 0 else (last_sect - u_boot_sectors - zImage_sectors)),
     'u-tools.img':          (u_tools_path,      (u_boot_sectors + zImage_sectors + ramdisk_uboot_sectors) if last_sect == 0 else (last_sect - u_boot_sectors - zImage_sectors - ramdisk_uboot_sectors)),
     'system.img':           (system_path,   0),                                     # partition no use offset,then set 0
     'userdata.img':         (userdata_path, 0),
    #'other.img':            (otherdata_path,0),                                     # no use (future for map data ???)
     }


# image types
image_types = {"INSTALL_IMAGE_RAW":1,"INSTALL_IMAGE_EXT2":2,"INSTALL_IMAGE_EXT3":3,"INSTALL_IMAGE_TARGZ":10}

# flags
image_flags = {"INSTALL_FLAG_RESIZE":0x1,"INSTALL_FLAG_ADDJOURNAL":0x2}

# image actions
image_actions = {"ACTION_MKFS_EXT3":0x1,"ACTION_MKFS_EXT2":0x2,"ACTION_MKFS_VFAT":0x4}


class usbImage:
    virtual = True
    crc32 = False

############################################################################
# 1. u-boot.bin
############################################################################
class u_boot(usbImage):
    virtual = False
    order = 1
    name =  "u-boot.bin"
    offset = images_path_offset["u-boot.bin"][1]
    type = image_types["INSTALL_IMAGE_RAW"]           # dest img type

############################################################################
# 2. zImage
############################################################################
class zImage(usbImage):
    virtual = False
    order = 2 
    name =  "zImage"
    offset = images_path_offset["zImage"][1]
    type = image_types["INSTALL_IMAGE_RAW"]           # dest img type

############################################################################
# 3. ramdisk-uboot.img
############################################################################
class ramdisk_uboot(usbImage):
    virtual = False
    order = 3
    name =  "ramdisk-uboot.img"
    offset = images_path_offset["ramdisk-uboot.img"][1]
    type = image_types["INSTALL_IMAGE_RAW"]           # dest img type

############################################################################
# 4. u-tools.img
############################################################################
class u_tools(usbImage):
    virtual = False
    order = 4
    name =  "u-tools.img"
    offset = images_path_offset["u-tools.img"][1]
    type = image_types["INSTALL_IMAGE_RAW"]           # dest img type

############################################################################
# 5. system.img
############################################################################
class system(usbImage):
    virtual = False
    order = 5
    name =  "system.img"
    type =  image_types["INSTALL_IMAGE_EXT2"]          # dest img type
    partition = "system"
    flags = image_flags["INSTALL_FLAG_RESIZE"] + image_flags["INSTALL_FLAG_ADDJOURNAL"]


############################################################################
# 6. userdata.img
############################################################################
class userdata(usbImage):
    virtual = False
    order = 6
    name =  "userdata.img"
    type =  image_types["INSTALL_IMAGE_EXT2"]          # dest img type
    partition = "data"
    flags = image_flags["INSTALL_FLAG_RESIZE"] + image_flags["INSTALL_FLAG_ADDJOURNAL"]
    

############################################################################
# 7. third_party
############################################################################
#class third_party(usbImage):
#    virtual = False
#    order = 7
#    partition = "third_party"
#    action = image_actions["ACTION_MKFS_EXT3"] liunote!!! 
