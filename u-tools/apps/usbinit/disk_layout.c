/* liu note (not use andriod libcutils)can use linux script read disk_layout.conf gen this source file!!!  visual will do it */
#if 0
device {
    path /dev/block/mmcblk0

    scheme mbr

# bytes in a disk sector (== 1 LBA), must be a power of 2!
    sector_size 512

    # What LBA should the partitions start at?
    start_lba 20000

    # Autodetect disk size if == 0
    num_lba 0

      partitions {

      recovery {
            active y
            type linux
            len 128M
	      }


      cache {
            type linux
            len 51M
	      }

      system {
            type linux
            len 128M
	      }

      data {
            type linux
            len 128M
	      }

      third_party {
            type linux
            len -1
	      }

    }
}

struct disk_info {
    char *device;
    uint8_t scheme;
    int sect_size;       /* expected sector size in bytes. MUST BE POWER OF 2 */
    uint32_t skip_lba;   /* in sectors (1 unit of LBA) */
    uint32_t num_lba;    /* the size of the disk in LBA units */
    struct part_info *part_lst;
    int num_parts;
};

struct part_info {
    char *name;
    uint8_t flags;
    uint8_t type;
    uint32_t len_kb;       /* in 1K-bytes */
    uint32_t start_lba;    /* the LBA where this partition begins */
};

#endif

#include "diskconfig.h"

#ifndef SOC_OMAP3
struct part_info mypart_info[/*5*/] = {
	{.name = "system",
  	 .flags = PART_ACTIVE_FLAG,
         .type  = PC_PART_TYPE_LINUX,
         .len_kb = 256*1024,//256 * 1024 *1024/1024
	},
	
	{.name = "recovery",
         .type  = PC_PART_TYPE_LINUX,
         .len_kb = 256*1024,//256 * 1024 *1024/1024
	},

	{.name = "cache",
         .type  = PC_PART_TYPE_LINUX,
         .len_kb = 128*1024, //51 * 1024 *1024/1024
	},

	{.name = "data",
         .type  = PC_PART_TYPE_LINUX  /*PC_PART_TYPE_DOS*/,
#ifdef SOC_SIRF_PRIMA         
         .len_kb = -1,/*128 * 1024 *1024/1024*/
#else
	 .len_kb = 1024*1024,/*128 * 1024 *1024/1024*/
#endif
         
	}/*,
	{.name = "other",
         .type  = PC_PART_TYPE_LINUX,
         .len_kb = -1
	}*/
};

struct disk_info mydisk_info = {
    .device = "/dev/mmcblk0",
    .scheme = PART_SCHEME_MBR,
    .sect_size = 512,
    .num_lba = 0,
    .skip_lba = 20000,
    /*.part_lst = &mypart_info,*/
    .num_parts = /*5*/4
};
#else
/* for omap3evm |fat32,Ext3,fat32| */
struct part_info mypart_info[/*5*/] = {
	{.name = "install",
  	 .flags = PART_ACTIVE_FLAG,
         .type  = PC_PART_TYPE_DOS,
         .len_kb = 256*1024,//256 * 1024 *1024/1024
	},
	
	{.name = "system",
         .type  = PC_PART_TYPE_LINUX,
         .len_kb = 256*1024,//256 * 1024 *1024/1024
	},

	{.name = "data",
         .type  = /*PC_PART_TYPE_LINUX*/ PC_PART_TYPE_DOS,

         .len_kb = -1,/*128 * 1024 *1024/1024*/
         
	}/*,
	{.name = "other",
         .type  = PC_PART_TYPE_LINUX,
         .len_kb = -1
	}*/
};

struct disk_info mydisk_info = {
    .device = "/dev/mmcblk0",        /* install dest dev */
    .scheme = PART_SCHEME_MBR,
    .sect_size = 512,
    .num_lba = 0,
    .skip_lba = /*20000*/1024*2,
    /*.part_lst = &mypart_info,*/
    .num_parts = /*5*/3
};

#endif

