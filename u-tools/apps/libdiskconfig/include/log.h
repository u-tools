
#ifndef __DUMMY_LOG_H
#define __DUMMY_LOG_H

/* liu */
//#define LOGE  printf
#define  LOGE(fmt, args...) \
                do{ \
                fprintf(stderr,fmt,##args); \
                fprintf(stderr, "\n"); \
        	}while(0)
        	
#define LOGI  printf
#define LOGW  printf


#define LOG_ALWAYS_FATAL_IF(cond, ...)


#endif /* __DUMMY_LOG_H */
