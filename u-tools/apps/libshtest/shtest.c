
#include <stdio.h>
#include <stdlib.h>

#include "include/shtest.h"

//#define EXTERN_TEST

//#define GLOBAL_TEST

//#define STATIC_FUNC_TEST   // now test no static, test call self extern func

#define  SIMPLE_FUNC_TEST

#ifdef STATIC_FUNC_TEST
/*static*/ int GetInt(int a)
{
if ( a <0)
return a*-1;
return a;
}
#endif

/* 
int shtest()
{
    printf(HELLO_);
    return 0;
}
*/

#ifdef EXTERN_TEST
extern int g_extern_test;
#endif

#ifdef GLOBAL_TEST
int g_global_test=0;
#endif

int GetMax(int a, int b)
{
#ifdef SIMPLE_FUNC_TEST
   return a+b;
#else
	
if (a >= b){
#ifdef EXTERN_TEST
	g_extern_test =1;
#endif	
#ifdef GLOBAL_TEST
        g_global_test=1;
#endif
#ifdef STATIC_FUNC_TEST
       return GetInt(a);
#else
        return a;
#endif        
}

#ifdef EXTERN_TEST
g_extern_test =0;
#endif

#ifdef GLOBAL_TEST
g_global_test =0;
#endif
#ifdef STATIC_FUNC_TEST
       return GetInt(a);
#else
return b;
#endif
#endif
}




