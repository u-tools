/* commands/sysloader/installer/installer.h
 *
 * Copyright 2008, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef __COMMANDS_SYSLOADER_INSTALLER_INSTALLER_H
#define __COMMANDS_SYSLOADER_INSTALLER_INSTALLER_H

#include <stdint.h>

/* image types */
#define INSTALL_IMAGE_RAW          1
#define INSTALL_IMAGE_EXT2         2
#define INSTALL_IMAGE_EXT3         3
#define INSTALL_IMAGE_TARGZ        10


/* flags */
#define INSTALL_FLAG_RESIZE        0x1
#define INSTALL_FLAG_ADDJOURNAL    0x2

/* installer mode */
#define INSTALL_DISK       0x1
#define INSTALL_USB_TMP    0x2
//#define INSTALL_USB_ADB  0x4

/* installer actions */

#define ACTION_MKFS_EXT3  0x1
#define ACTION_MKFS_EXT2  0x2
#define ACTION_MKFS_VFAT  0x4


/* liunote tmp ,future use dnw or adb download */
#define UBOOT_TMP_PARTITION   "third_party"          /* uboot caculate it offset in secotors !!!*/


typedef struct _usb_image_info_ {        /* 32 bytes total*/
    char part_name[16/*+2*/];      /* no +2 for 4byte aligned */
    uint8_t flags;
    uint8_t type;
    uint32_t offset;
    uint32_t size;         /* in bytes */
    uint32_t sectors;      /* in sectors */
    uint32_t checksum;     /* check sum */
    uint16_t actions;       /* mkfs ext3,mkfs vfat,etc...*/
}usb_image_info;

typedef struct _usb_install_info_ {
    //char device[24];            /* tmp images device name ,liunote use it ??? */
    uint32_t magic;                /* 0x5F646E77 'dnw_' */
    uint32_t start_tmp_offset;    /* if usb bulk at kernel ,doesnt need this filed!!! ,in sectors */
    //int sect_size;              /* expected sector size in bytes. MUST BE POWER OF 2 , now is 512 bytes */
    uint32_t num_images;
    usb_image_info image_lst[0];
}usb_install_info;


int installer(int mode,int binit);

#endif /* __COMMANDS_SYSLOADER_INSTALLER_INSTALLER_H */

