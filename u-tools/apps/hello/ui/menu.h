#ifndef _MENU_H_
#define _MENU_H_


#define MENU_STARTING_ROW                       5        // start form 5 row
#define MENU_WRAP_AROUND_ENABLE                 TRUE     // select cycle

#define MENU_LIST \
		    MENU_MAIN,\
		    MENU_SETUP,\
		    MENU_DIAG
		
//#include "tui.h"            // must define in this place!

		
//define func macro
#define MENU_ACTION_INSTALLER         &ActionInstaller
#define INIT_DISK_ARG          0
#define FORMAT_DISK_ARG        1
#define USB_INSTALL_ARG        2
#define DISK_INSTALL_ARG       3
// extern function

	
// define each menu that appears in the above list as a MENU_DESC(name) followed by MENU_TEXT and MENU_ITEM entries
//MENU_ITEM   
#define INSTANTIATE_MENU_INFO \
		MENU_DESC(MENU_MAIN) \
		MENU_TEXT("main menu") \
		MENU_TEXT("") \
		MENU_TEXT("[Use key select and enter]") \
		MENU_TEXT("----------------------------") \
		MENU_ITEM("* Use usb install ",/*MENU_ACTION_NONE*/MENU_ACTION_INSTALLER ,USB_INSTALL_ARG, MENU_ENABLE) \
		MENU_ITEM("* Use sdcard install ",/*MENU_ACTION_NONE*/MENU_ACTION_INSTALLER ,DISK_INSTALL_ARG, MENU_ENABLE) \
		MENU_ITEM(". System setup",MENU_ACTION_GOTO_MENU , MENU_SETUP , MENU_ENABLE) \
		MENU_ITEM(". System diagnostic",MENU_ACTION_GOTO_MENU , MENU_SETUP , MENU_ENABLE) \
		MENU_TEXT("----------------------------") \
		\
		MENU_DESC(MENU_SETUP) \
		MENU_TEXT("Setup menu") \
		MENU_TEXT("") \
		MENU_TEXT("[use key select and enter]") \
		MENU_TEXT("----------------------------") \
		MENU_ITEM(".return",MENU_ACTION_GOTO_MENU, MENU_MAIN, MENU_ENABLE) \
		MENU_TEXT("----------------------------") \
                \
		MENU_DESC(MENU_DIAG) \
		MENU_TEXT("Diagnostic menu") \
		MENU_TEXT("") \
		MENU_TEXT("[use key select and enter]") \
		MENU_TEXT("----------------------------") \
		MENU_ITEM(".return",MENU_ACTION_GOTO_MENU, MENU_MAIN, MENU_ENABLE) \
		MENU_TEXT("----------------------------") 

#endif  //_MENU_H_
