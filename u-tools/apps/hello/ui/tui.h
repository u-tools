#ifndef _TUI_H_
#define _TUI_H_

//*************
// menu
//*************
//***************************************************************************
// MISC
//****************************************************************************
#define TEXT_JUSTIFICATION_CENTER	0
#define TEXT_JUSTIFICATION_LEFT		1
#define TEXT_JUSTIFICATION_RIGHT	2

#define TEXT_ATTRIBUTE_NORMAL		  0
#define TEXT_ATTRIBUTE_INVERSE		1

#ifndef TRUE
#define TRUE   1
#endif
#ifndef FALSE
#define FALSE  0
#endif


typedef struct __TUIPARAMS
{
	int 			cxScreen;                    
	int 			cyScreen;
	int 			cxLogicalScreen;
	int 			cyLogicalScreen;
	
	int				nColumns;                    //text rows and cols
	int				nRows;                       
	int				nTextHeight;                 //at font.h    8x16 or 8x8
	int				nTextWidth;                  
	int				cbStride;                    //
	int				nFormat;                     //=cBpp
	int				cBpp;                        //1,2,4,8,//16???  Bit per pixel
	int				nRotation;                   //0,90,180,270
} TUI_PARAMS;




//****************************************************************************
// user input 
//****************************************************************************
typedef enum {
    USER_INPUT_NONE,
    //USER_INPUT_MENU,
    USER_INPUT_UP,
    USER_INPUT_DOWN,
    //USER_INPUT_NEXT,
    //USER_INPUT_PREVIOUS,
    USER_INPUT_SELECT,
    USER_INPUT_ESCAPE,
    USER_INPUT_TIMEOUT
    //USER_INPUT_MANUFACTURING_DIAGNOSTICS
} USER_INPUT_CODE;                              //define some key input msg

typedef enum {
	STATE_INITIAL,
	STATE_DEBOUNCE_NONE,
	STATE_IDLE,
	STATE_DEBOUNCE_PRESS,
	STATE_DEBOUNCE_RELEASE
} GET_MENU_INPUT_STATE;                         //define some menu key option statu
//****************************************************************************

//****************************************************************************
// menu
//****************************************************************************
#define MENU_ENABLE                             TRUE 
#define MENU_DISABLE                            FALSE

//define menu fix process function name 
#define MENU_ACTION								  void *
#define MENU_ACTION_FAULT						&ActionFaultMenu          // fault proc func
#define MENU_ACTION_GOTO_MENU				&ActionGotoMenu           // main menu switch  (support two level menu)
#define MENU_ACTION_NONE						&ActionNoneMenu           // dummy menu
#define MENU_ACTION_NOT_SUPORTED		&ActionNotSupportedMenu   // func for menu dont support station

#define MENU_DESC(desc)							{NULL, NULL, desc, 1},
#define MENU_TEXT(text)							{text, NULL, 0, 1},
#define MENU_ITEM(text, action, param, visible) {text, action, param, visible},

typedef struct menuline_info_t {
	char *pText;                                // menu name
	void (*pAction)(int argument);              // menu func pointer
	unsigned int Param;                         // menu func param
	unsigned int visible;		// non-zero to display line within menu  //set menu invisiable
} MENULINE_INFO, *MENULINE_INFO_PTR;

// move to below 
//typedef enum {
//NO_MORE_MENUS = 0,		/* must be last MENU_DESC in menus list */
//	MENU_LIST             // very classes!!!
//} MENUS;

//forward
void  ActionFaultMenu (int argument);         // fault proc
void  ActionGotoMenu  (int argument);         // main menu switch  (support two level menu)
void  ActionNoneMenu  (int argument);         // dummy menu
void  ActionNotSupportedMenu (int argument);  // func for menu dont support station

#include "menu.h"

typedef enum {
    NO_MORE_MENUS = 0,		/* must be last MENU_DESC in menus list */
  	MENU_LIST             // very classes!!!
} MENUS;

#ifndef MENU_STARTING_ROW
#define MENU_STARTING_ROW                       0        //start form 0 row 
#endif 
#ifndef MENU_WRAP_AROUND_ENABLE
#define MENU_WRAP_AROUND_ENABLE                 TRUE     //select cycle
#endif 


// extern func
void setMenus(MENULINE_INFO_PTR menus,int menusSize,MENUS startMenu);

void GetUserActionFromMenu (MENUS MenuNumber);    // get user active menu item

void DisplayString(int row, char *string, int justification, int width, int inverse);





//****************************************************************************
//#if 0
//****************************************************************************
// dialog
//****************************************************************************
#define DLGITEM_ENABLE                         TRUE 
#define DLGITEM_DISABLE                        FALSE

#ifndef DLG_STARTING_ROW
#define DLG_STARTING_ROW                       0        // start from 0
#endif 
#ifndef DLG_WRAP_AROUND_ENABLE
#define DLG_WRAP_AROUND_ENABLE                 TRUE     // select cycle
#endif 

#define DLGITEM_GETINT                         0x00000000
#define DLGITEM_GETFLOAT                       0x00010000
#define DLGITEM_GETCASH                        0x00020000
#define DLGITEM_GETSTR                         0x00030000
#define DLGITEM_GETFIXSTR                      0x00040000  // fix len
#define DLGITEM_GETDATETIME                    0x00050000
#define DLGITEM_GETMASK                        0xffff0000
#define DLGITEM_LENMASK                        0x0000ffff



//define dlg fix proc func
#define DLGITEM_ACTION							  void *
#define DLGITEM_ACTION_FAULT					&ActionFaultDlgItem            
#define DLGITEM_ACTION_RETURN      		ActionReturnDlgItem      
#define DLGITEM_ACTION_NONE						&ActionNoneDlgItem 
#define DLGITEM_ACTION_NOT_SUPORTED		&ActionNotSupportedDlgItem

//#define DLG_DESC(desc)						        {NULL, NULL,NULL, desc, 1},
#define DLG_TEXT(text)							        {text, NULL,NULL, 0,NULL, 1},
#define DLG_ITEM(text,action, content,param, visible)   {text, action, content,param,NULL, visible},
#define DLG_BUTTON(text,action, param, visible)         {text, action, NULL,0,param, visible},

typedef struct dlgline_info_t {
	char *pText;
	void (*pAction)(int argument1,void *argument2);
    char *pContent;
	unsigned int Param1;
	void * Param2;
	unsigned int visible;		                    // non-zero to display line within dlg item 
} S_DLGLINE_INFO, *S_DLGLINE_INFO_PTR;

typedef enum {
	//DLG_TXT_TIP = 0,		/* must be last MENU_DESC in menus list */
	DLGITEM_LIST            
} DLGITEMS; 


//****************************************************************************

// extern func
void setDlg(S_DLGLINE_INFO_PTR DlgItems,int DlgItemsSize,DLGITEMS startDlgItem);


void GetUserActionFromDlg (DLGITEMS  DlgItemNumber);
//****************************************************************************
//#endif  
//****************************************************************************

//all  other extern function
//****************************************************************************
// MsgBox
//****************************************************************************
//BOOL msgBox(char * message);

USER_INPUT_CODE  GetUserInput (unsigned long TimeoutValue);

int tui_init(__u32 xres,__u32 yres,__u32 rotate,int font_width,int font_hight,int disp_bpp);

#endif  //_TUI_H_
