
#include <stdio.h>
//#include <stdarg.h>
#include <stdlib.h>
#include <string.h>

#include <fcntl.h>
#include <sys/mman.h>
#include <errno.h>
#include <linux/input.h>

//#include <linux/delay.h>
#include <time.h>
    
#include  "fbutils.h"
#include  "tui.h"

//#define DEBUG_UI  

#define MAXTXTLINE  9             
#define MAXTXTCOL   26 

typedef struct _ts_sample_ {
	int		x;
	int		y;
	unsigned int	pressure;
	struct timeval	tv;
}TS_SAMPLE;


TUI_PARAMS tui_params;

//TS_SAMPLE ts_sample;


static struct input_event ev;

static int current_x = 0, current_y = 0, current_p = 0;

int ts_fd;
//int cal_buffer[16];
//int mode = TS_SETMODE;



static int ts_read(int fd,TS_SAMPLE *samp)
{
	int ret;
	unsigned char *p = (unsigned char *) &ev;
	int len = sizeof(struct input_event);

	while(1) {
		ret = read(fd, p,len);
		if (ret == -1) {
			if (errno == EINTR) {
				printf("ts_read: read err\n");
				continue;
			}
			break;
		}

		if (ret < (int)sizeof(struct input_event)) {
			p += ret;
			len -= ret;
			continue;
		}
#ifdef DEBUG_UI		
		printf("ev.type=%d, ev.code=%d,ev.value=%d\n",ev.type,ev.code,ev.value);
#endif		
		
		
		if (ev.type == EV_ABS) {/*3*/
			switch (ev.code) {
			case ABS_X:
				if (ev.value != 0) {
					samp->x = current_x = ev.value;
					samp->y = current_y;
					samp->pressure = current_p;
				} else {
					continue;
					}
				break;
			case ABS_Y:
				if (ev.value != 0) {
					samp->x = current_x;
					samp->y = current_y = ev.value;
					samp->pressure = current_p;
				} else {
					continue;
					}
				break;
			case ABS_PRESSURE:
				samp->x = current_x;
				samp->y = current_y;
				samp->pressure = current_p = ev.value;
				break;
			}
			samp->tv = ev.time;
		} else if (ev.type == EV_KEY) {/*1*/
			switch (ev.code) {
			case BTN_TOUCH:
				if (ev.value == 0) {
					/* pen up */
					samp->x = 0;
					samp->y = 0;
					samp->pressure = 0;
					samp->tv = ev.time;
				}
				else {
					samp->x = current_x;
					samp->y = current_y;
					#ifdef SOC_SIRF_PRIMA
					samp->pressure = current_p;  // pen down???
					#else 
					//#error no prima
					samp->pressure = current_p = ev.value;
					#endif
					samp->tv = ev.time;
				}
				break;
			}
		}else if(ev.type == EV_SYN) {/*0*/
				samp->x = current_x;
				samp->y = current_y;
				samp->pressure = current_p;
				samp->tv = ev.time;
		}
		return 0;
	}
	return -1;
}


static int clearbuf(int fd)
{
	fd_set fdset;
	struct timeval tv;
	int nfds;
	TS_SAMPLE sample;

	while (1) {
		FD_ZERO(&fdset);
		FD_SET(fd, &fdset);

		tv.tv_sec = 0;
		tv.tv_usec = 0;

		nfds = select(fd + 1, &fdset, NULL, NULL, &tv);
		if (nfds == 0) break;

		ts_read(fd, &sample);
	}
	return 0;
}	

#if 0
static int getxy(int fd,int *x,int *y)
{
#define MAX_SAMPLES 128
	TS_SAMPLE samp[MAX_SAMPLES];
	int ret = -1;
	int index, middle;
	do {
		if (ts_read(fd, &samp[0]) < 0) {
			return -1;
		}
		
	} while (samp[0].pressure == 0);

	/* Now collect up to MAX_SAMPLES touches into the samp array. */
	index = 0;
	do {
		if (index < MAX_SAMPLES-1)
			index++;
		if (ts_read(fd, &samp[index]) < 0) {
			return -1;
		}
	} while (samp[index].pressure > 0);

	middle = index/2;
	if (x) {
		qsort(samp, index, sizeof(struct ts_sample), sort_by_x);
		if (index & 1)
			*x = samp[middle].x;
		else
			*x = (samp[middle-1].x + samp[middle].x) / 2;
	}
	if (y) {
		qsort(samp, index, sizeof(struct ts_sample), sort_by_y);
		if (index & 1)
			*y = samp[middle].y;
		else
			*y = (samp[middle-1].y + samp[middle].y) / 2;
	}
	return 0;
}
#endif

USER_INPUT_CODE  GetUserInput (unsigned long TimeoutValue)
{
	TS_SAMPLE samp;
	int ret = -1;
	/*suseconds_t*/ time_t press_time, up_time,tmp_time;
	int times ;
	struct timeval tv,tv2;
	
	struct timespec tpstart;
        struct timespec tpend;
        long timedif;

        
        //printf("GetUserInput\n");
loop1:	
	clearbuf(ts_fd);
	printf("GetUserInput\n");
	do {
		if (ts_read(ts_fd, &samp) < 0) {
			//return -1;
			continue;
		}
		
	} while ((samp.pressure == 0) );
	
	gettimeofday(&tv,NULL);
	press_time = tv.tv_sec;
	printf("start press_time: %ld\n",press_time);
	
	//printf("start :%ld\n",/*mktime(localtime(&tmp_time))*/time((time_t*)NULL));
	// pen down start
	//press_time = samp.tv.tv_sec; //tv_usec;
	printf("start :%ld\n",samp.tv.tv_sec);
	
	
	//clock_gettime(CLOCK_MONOTONIC, &tpstart);

#ifdef SOC_SIRF_PRIMA
#else	
	times =5;
	do {
		if (ts_read(ts_fd, &samp) < 0) {
			//return -1;
			//continue;
			printf("ts_read err1\n");
			goto loop1;
		}
		if (samp.pressure){
			times--;
		}
		else{
		    printf("ts_read err2\n");
		    goto loop1;
		}
		
	} while (/*(samp.pressure > 0) &&*/ (times != 0) );
#endif	
	
	printf("ture press!\n");
        
loop2:	
	// wait pen up
	do {
		if (ts_read(ts_fd, &samp) < 0) {
			//return -1;
			//continue;
			printf("ts_read err3\n");
			goto loop1;
		}
		usleep(5);
	} while ((samp.pressure > 0));
	
#if 0	
	times =5;
	do {
		if (ts_read(ts_fd, &samp) < 0) {
			//return -1;
			continue;
		}
		if (samp.pressure ==0 ){
			times--;
		}
		else
		    goto loop2;
		
	} while ((samp.pressure == 0) && (times == 0) );
#endif	
	

	
	//up_time = samp.tv.tv_sec; //tv_usec;
	
	gettimeofday(&tv2,NULL);
	//printf("up: %06d\n", (int)tv.tv_sec);
	up_time=tv2.tv_sec;
	
	//printf("end :%ld\n",/*mktime(localtime(&tmp_time))*/time((time_t*)NULL));
	printf("start :%ld\n",samp.tv.tv_sec);
	
	//clock_gettime(CLOCK_MONOTONIC, &tpend);
        //timedif = 1000000*(tpend.tv_sec-tpstart.tv_sec)+(tpend.tv_nsec-tpstart.tv_nsec)/1000;
        //printf("it took %ld microseconds\n", timedif);


#ifdef DEBUG_UI		
	printf("press_time=%ld,up_time=%ld,eclapse=%ld\n",press_time,up_time,up_time-press_time);
#endif	
	
	if(up_time - press_time >= 2l)
		return USER_INPUT_SELECT;
	else
		return USER_INPUT_DOWN;

}

int tui_init(__u32 xres,__u32 yres,__u32 rotate,int font_width,int font_hight,int disp_bpp)
{
	  tui_params.cxScreen = xres;
    tui_params.cyScreen = yres;
    
    printf("xres=%d,yres=%d\n",xres,yres);
 
    // initialize display description
    tui_params.nRotation = rotate;

    if ((rotate == 90) || (rotate == 270)){
    	tui_params.cxLogicalScreen = tui_params.cyScreen;
      tui_params.cyLogicalScreen = tui_params.cxScreen;
    }
    else{
    	tui_params.cxLogicalScreen = tui_params.cxScreen;
      tui_params.cyLogicalScreen = tui_params.cyScreen;
    }

    tui_params.nColumns = tui_params.cxLogicalScreen / font_width;
    tui_params.nRows = tui_params.cyLogicalScreen / font_hight;
    tui_params.nTextHeight = font_hight;                              
    tui_params.nTextWidth = font_width;


    tui_params.cBpp = disp_bpp;                                        
    //tui_params.cbStride = DISP_STRIDE * sizeof (DWORD);
    tui_params.nFormat = disp_bpp;
    
    ts_fd = open(/* "/dev/input/event1" */"/dev/event1",O_RDONLY);
    if(ts_fd == -1) {
    	printf("Open ts device failed");
		  return 1;
	  }
    
    return 0;

}

void DisplayString(int row, char *string, int justification, int width, int inverse)
{
	int col,rowPixelSize,strPixelSize ,i;
	strPixelSize = strlen(string);
	strPixelSize = (strPixelSize * tui_params.nTextWidth)>tui_params.cxLogicalScreen ? tui_params.cxLogicalScreen : (strPixelSize*tui_params.nTextWidth);
  switch (justification)
  {
  	case TEXT_JUSTIFICATION_CENTER:
  		col=(tui_params.cxLogicalScreen-strPixelSize)/2;
		  break;
	  case TEXT_JUSTIFICATION_LEFT:
		  col=0;
		  break;
	  case TEXT_JUSTIFICATION_RIGHT:
		  col=(tui_params.cxLogicalScreen-strPixelSize);
		  break;
  }
  
  rowPixelSize = tui_params.nTextHeight * row;
  //printf("rowPixelSize=%d,col=%d\n",rowPixelSize,col);
  
  // liunote clear
  for(i=0; i< (tui_params.cxLogicalScreen/tui_params.nTextWidth); i++)
     put_string (i*tui_params.nTextWidth,rowPixelSize, " ", 1,0);
  
  if(inverse)
        put_string (col,rowPixelSize, string,0,1);
  else
  	put_string (col,rowPixelSize, string, 1,0);
  	
}


//***************
// menu 
//***************
int MenuLinesWritten = 0;
//define menu key statu
//GET_MENU_INPUT_STATE MenuInputState = STATE_INITIAL;
static  MENULINE_INFO_PTR   __menus__;
static  MENUS               __startMenu__;
static  int                 __menusSize__;


void setMenus(MENULINE_INFO_PTR menus,int menusSize,MENUS startMenu)
{
	__menus__=menus;
	 __menusSize__=menusSize;
	__startMenu__=startMenu;
}

static void ClearMenuArea (int NumLines)
{
    int i;
    int StartingRow = MENU_STARTING_ROW;
    
    

    // clear previous menu, keeping the rest of the bootloader messages at bottom
    for (i = 0; i < (NumLines + 1); i++)
	  DisplayString (StartingRow++, "                                        ", TEXT_JUSTIFICATION_CENTER, 0, TEXT_ATTRIBUTE_NORMAL);  //???
}

/* 
 * Find start index in menus list for a given menu number
 */
static int FindMenuStartIndex (MENUS MenuNumber)  //menus include menuItems struct  //get correct index
{
    int index;

    for (index = 0; index <__menusSize__ /*(sizeof (__menus__) / sizeof (S_MENULINE_INFO))*/;index++){
	    //printf("pAction %x, pText %x, Param %x, MenuNum %x\r\n", menus[index].pAction, menus[index].pText, menus[index].Param, MenuNumber);
	    // Look for MENU_DESC containing MenuNumber
	    if ((__menus__[index].pAction == NULL) && (__menus__[index].pText == NULL)
		&& ((MENUS) __menus__[index].Param == MenuNumber)){
		    index++;	//move to first entry in menu
		    return index;
		}
	}
    return -1;
}

/*
 * GetNumItemsInMenu() - Examine raw menu data and figure out how many menu items exist
 * in any given menu within the data structure.
 */
static int GetNumItemsInMenu (MENUS MenuNumber)
{
    int NumItems = 0;
    int index = 0;

    index = FindMenuStartIndex (MenuNumber);
    if (index < 0)
	return -1;

    // search until find MENU_DESC
    while (!((__menus__[index].pAction == NULL) && (__menus__[index].pText == NULL))){
	    // count only at visible, MENU_ITEMs
	    if (((__menus__[index].pAction != NULL)
		    && (__menus__[index].pText != NULL)) && __menus__[index].visible){
		    NumItems++;
		}

	    index++;
	}
    return NumItems;
}

/*
 * GetMaxMenuItemWidth() - Examine raw menu data and figure out maximum text width of the
 * given MenuNumber.
 */
static int GetMaxMenuItemWidth (MENUS MenuNumber)
{
    int MaxLength = 0;
    int index = 0;

    index = FindMenuStartIndex (MenuNumber);
    if (index < 0)
	return -1;

    // search until find MENU_DESC
    while (!((__menus__[index].pAction == NULL) && (__menus__[index].pText == NULL))){
	    if (MaxLength < (int) strlen (__menus__[index].pText)){
		    MaxLength = (int) strlen (__menus__[index].pText);
		}
	    index++;
	}
    return MaxLength;
}

static int DrawMenu (MENUS MenuNumber, int StartingRow, int Selection)
{
    int NumItems = 0;
    int index = 0;
    int CurrentMenuIndex = 0;

    MenuLinesWritten = 0;

    index = FindMenuStartIndex (MenuNumber);
    if (index < 0)
	return -1;
	
    // search until find MENU_DESC
    while (!((__menus__[index].pAction == NULL) && (__menus__[index].pText == NULL))){
	    // display MENU_TEXT    // display tip text
	    if ((__menus__[index].pAction == NULL)
		&& (__menus__[index].pText != NULL)){
		    // Display MENU_TEXT
		    DisplayString (StartingRow++, __menus__[index].pText,/*TEXT_JUSTIFICATION_LEFT*/TEXT_JUSTIFICATION_CENTER, GetMaxMenuItemWidth (MenuNumber),
			TEXT_ATTRIBUTE_NORMAL);
		    MenuLinesWritten++;
		}
	    // display MENU_ITEM
	    else{
		    if (__menus__[index].visible){	// only display if visible
			    if (NumItems++ == Selection){
				    DisplayString (StartingRow++,
					__menus__[index].pText,
					/*TEXT_JUSTIFICATION_LEFT,*/
					TEXT_JUSTIFICATION_CENTER,
					GetMaxMenuItemWidth (MenuNumber),
					TEXT_ATTRIBUTE_INVERSE);
				    CurrentMenuIndex = index;
				}
			    else{
				    // Display MENU_ITEM
				    DisplayString (StartingRow++,
					__menus__[index].pText,
					/*TEXT_JUSTIFICATION_LEFT,*/
					TEXT_JUSTIFICATION_CENTER,
					GetMaxMenuItemWidth (MenuNumber),
					TEXT_ATTRIBUTE_NORMAL);
				}
			    MenuLinesWritten++;
			}
		}
	    index++;
	}
    return CurrentMenuIndex;
}


void GetUserActionFromMenu (MENUS MenuNumber)    // get user active menu item
{
    int Selection = 0;
    int CurrentMenuIndex = 0;
    int StartingRow = MENU_STARTING_ROW;
    int ExitMenu = 0;
    USER_INPUT_CODE  UserInput;
    
    clearbuf(ts_fd);

    while(!ExitMenu){
	    // Display the menu
	    CurrentMenuIndex = DrawMenu (MenuNumber, StartingRow, Selection);
	    // check if accidently jumped to menu number not in menus list
	    if (CurrentMenuIndex < 0){
		    return;
		}

	    UserInput = GetUserInput(0);

	    switch (UserInput){
		    //case MENU_INPUT_TIMEOUT:
			//return;
			//break;

		    case USER_INPUT_UP:
			///bAnyUserInput = TRUE;
			if (--Selection < 0)
			    {
#if MENU_WRAP_AROUND_ENABLE
				//Selection = GetNumItemsInMenu(MenuNumber) - 1;
				Selection = GetNumItemsInMenu (MenuNumber);
				if (Selection)	//allow for 0 items in menu list
				    Selection--;
#else
				Selection = 0;
#endif
			    }
			break;

		    case USER_INPUT_DOWN:
			///bAnyUserInput = TRUE;
			if (++Selection >= GetNumItemsInMenu (MenuNumber))
			    {
#if MENU_WRAP_AROUND_ENABLE
				Selection = 0;
#else
				//Selection = GetNumItemsInMenu(MenuNumber) - 1;
				Selection = GetNumItemsInMenu (MenuNumber);
				if (Selection)	//allow for 0 items in menu list
				    Selection--;
#endif
			    }
			break;

		    //case MENU_INPUT_NEXT:
		    case USER_INPUT_SELECT:
			(*__menus__[CurrentMenuIndex].pAction) (__menus__[CurrentMenuIndex].Param);
			break;

		    //case MENU_INPUT_PREVIOUS:
		    case USER_INPUT_ESCAPE:
			if (MenuNumber != /*gStartingMenu*/__startMenu__)
			    {
				ClearMenuArea (MenuLinesWritten);
				return;
			    }
			break;

		    default:
			break;
		}
	    ///Timeout = 0;
	}
}

void ActionGotoMenu (int argument)
{
    // go one layer deeper into menu system
    // argument passed is the menu number to bring up
    ClearMenuArea (MenuLinesWritten);
    MenuLinesWritten = 0;
    GetUserActionFromMenu ((MENUS) argument);
}

void  ActionFaultMenu (int argument)
{

}
void  ActionNoneMenu  (int argument)
{

}
void  ActionNotSupportedMenu (int argument)
{

}


//#if 0
//***************
// dlg
//***************
int DlgLinesWritten = 0;
//GET_MENU_INPUT_STATE MenuInputState = STATE_INITIAL;
static  S_DLGLINE_INFO_PTR __dlgItems__;
static  DLGITEMS           __startDlgItem__;
static  int                __dlgItemsSize__;
int  curSrcLine=1;         // direct current select line actually 

static void ClearDlgArea (int NumLines)
{
    int i;
    int StartingRow = DLG_STARTING_ROW;

    // clear previous menu, keeping the rest of the bootloader messages at bottom
    for (i = 0; i < (NumLines /*+ 1*/); i++)
	  DisplayString (StartingRow++, "                          ", TEXT_JUSTIFICATION_CENTER, 0, TEXT_ATTRIBUTE_NORMAL);  //???
}

/* 
 * Find start index in dlgitem list for a given dlgItem number
 */
static int FindDlgStartIndex (DLGITEMS  DlgItemNumber)  
{
    int index;

    //for (index = 0; index <__dlgItemsSize__ /*(sizeof (__dlgItem__) / sizeof (S_DLGLINE_INFO))*/;index++){
	//    if ((__dlgItems__[index].pAction == NULL) && (__dlgItems__[index].pText == NULL)
	//	&& ((DLGITEMS) __dlgItems__[index].Param == DlgItemNumber)){
	//	    index++;	//move to first entry in dlgitem
	//	    return index;
	//	}
	//}
	
    index=DlgItemNumber;
    if(curSrcLine>=MAXTXTLINE){
	  index++;
	  if(index>=__dlgItemsSize__)
	     index=__dlgItemsSize__-1;
      curSrcLine--;
      ClearDlgArea (MAXTXTLINE);   
    }
   
    if(curSrcLine<1){
      index--;
   	  if(index<=0)
	     index=0;
      curSrcLine++;
      ClearDlgArea (MAXTXTLINE);  
    }
    return index;
}


static int DrawDlg (DLGITEMS DlgItemNumber, int StartingRow, int Selection)
{
    char scrLine[MAXTXTCOL+1];          
    //int NumItems = 0;
    int index = 0;
    int CurrentDlgItemIndex = 0;

    DlgLinesWritten = 0;
    
    //display dlg name
    DisplayString (StartingRow++, "dlg window",/*TEXT_JUSTIFICATION_LEFT*/TEXT_JUSTIFICATION_CENTER,8,
    	TEXT_ATTRIBUTE_NORMAL);
        DlgLinesWritten++;

	  index =DlgItemNumber ;
    
    if (index < 0)
	  return -1;
	
    while (StartingRow <MAXTXTLINE/*!((__dlgItems__[index].pAction == NULL) && (__dlgItems__[index].pText == NULL))*/){
    	    if (__dlgItems__[index].visible){	// only display if visible
    		    if(__dlgItems__[index].pContent==NULL){   // button condition
    		      strcpy(scrLine,"[");
    		      strcat(scrLine,__dlgItems__[index].pText);
    		      strcat(scrLine,"]");
		        }
		        else{
     		      strcpy(scrLine,__dlgItems__[index].pText);
    		      strcat(scrLine,":");
    		      if(MAXTXTCOL-strlen(__dlgItems__[index].pContent)-strlen(__dlgItems__[index].pText)>0)
        		      strcat(scrLine,__dlgItems__[index].pContent);
        		  else
        		      strcat(scrLine,"...");        //too long dont display
		        }
		    
			    if (/*NumItems++*/index == Selection){
				    DisplayString (StartingRow++,
					/*__dlgItems__[index].pText,*/
					scrLine,
					TEXT_JUSTIFICATION_LEFT,
					strlen(scrLine)/*GetMaxDlgItemWidth (index)*/,
					TEXT_ATTRIBUTE_INVERSE);
				    CurrentDlgItemIndex = index;
				}
			    else{
				    // Display dlg_ITEM
				    DisplayString (StartingRow++,
					/*__dlgItems__[index].pText,*/
					scrLine,
					TEXT_JUSTIFICATION_LEFT,
					strlen(scrLine)/*GetMaxDlgItemWidth (index)*/,
					TEXT_ATTRIBUTE_NORMAL);
				}
			    DlgLinesWritten++;
			}
		
	    index++;
	}
    return CurrentDlgItemIndex;
}


void GetUserActionFromDlg (DLGITEMS  DlgItemNumber)
{
    int Selection = 0;
    int CurrentDlgItemIndex = 0;
    int StartingRow = DLG_STARTING_ROW;
    int ExitDlg = 0;
    USER_INPUT_CODE  UserInput;
    
  
    while(!ExitDlg){
	    // Display the dlg items
	    DlgItemNumber=FindDlgStartIndex (DlgItemNumber); 
	     
	    CurrentDlgItemIndex = DrawDlg (DlgItemNumber, StartingRow, Selection);
	    // check if accidently jumped to menu number not in menus list
	    if (CurrentDlgItemIndex < 0){
		    return;
		}

	    UserInput = GetUserInput(0);

	    switch (UserInput){
		    //case MENU_INPUT_TIMEOUT:
			//return;
			//break;

		    case USER_INPUT_UP:
			///bAnyUserInput = TRUE;
			if (--Selection < 0){
//#if MENU_WRAP_AROUND_ENABLE
//				//Selection = GetNumItemsInMenu(MenuNumber) - 1;
//				Selection = __dlgItemsSize__;//GetNumItemsInDlg (DlgItemNumber);
//				if (Selection)	//allow for 0 items in menu list
//				    Selection--;
//#else
				Selection = 0;
//#endif
			}
			else{
			    curSrcLine--;    
			}
			break;

		    case USER_INPUT_DOWN:
			if (++Selection >= __dlgItemsSize__/*GetNumItemsInDlg (DlgItemNumber)*/){
//#if DLG_WRAP_AROUND_ENABLE
//				Selection = 0;
//#else
				//Selection = GetNumItemsInDlg(MenuNumber) - 1;
				Selection = __dlgItemsSize__;//GetNumItemsInDlg (DlgItemNumber);
				if (Selection)	//allow for 0 items in menu list
				    Selection--;
//#endif
			}
			else{
    			curSrcLine++;
			}    
			break;

		    //case MENU_INPUT_NEXT:
		    case USER_INPUT_SELECT:
		    ClearDlgArea (MAXTXTLINE);   
		    //判断是按钮还是输入项目
		    if(__dlgItems__[Selection].pContent==NULL)
    			(*__dlgItems__[/*CurrentDlgItemIndex*/Selection].pAction) (__dlgItems__[Selection].Param1,(void *)__dlgItems__[Selection].Param2);
    	    else                                          // input dlg box ???
    	        (*__dlgItems__[/*CurrentDlgItemIndex*/Selection].pAction) (__dlgItems__[Selection].Param1,(void *)__dlgItems__[Selection].pContent);
			break;

		    //case MENU_INPUT_PREVIOUS:
		    case USER_INPUT_ESCAPE:
			ClearDlgArea (/*DlgLinesWritten*/MAXTXTLINE);   //???
			return;
			break;

		    default:
			break;
		}
	    ///Timeout = 0;
	}
}


void ActionFaultDlgItem(int argument)
{
}

void ActionReturnDlgItem (int argument)         //???
{
    ClearDlgArea (DlgLinesWritten);             //??????
    DlgLinesWritten = 0;
    GetUserActionFromDlg ((DLGITEMS) argument);
}

void  ActionNoneDlgItem  (int argument)
{
}

void  ActionNotSupportedDlgItem (int argument)
{
}


void setDlg(S_DLGLINE_INFO_PTR DlgItems,int DlgItemsSize,DLGITEMS startDlgItem)
{
	__dlgItems__=DlgItems;
    __startDlgItem__=startDlgItem;
    __dlgItemsSize__=DlgItemsSize;
}

