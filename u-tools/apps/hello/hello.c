
#include <stdio.h>

// for pipe
#include <unistd.h>
#include <stdlib.h>
#include <string.h>

#include <sys/msg.h>
#include <sys/mount.h>
#include <errno.h>

#include <fcntl.h>

//#include <hello/hello.h>
#include <dlfcn.h>

#include "hello.h"

//#include <cutils/log.h>
//#include <log/android/log.h>

#include  "ui/fbutils.h"

#include  "installer/installer.h"

static int palette [] =
{
	0x000000, 0xffe080, 0xffffff, 0xe0c0a0
};

#define NR_COLORS (sizeof (palette) / sizeof (palette [0]))


#define MENU_STARTING_ROW                       0        // start form 0 row
#define MENU_WRAP_AROUND_ENABLE                 TRUE     // select cycle


//#define MENU_LIST \
//		    MENU_MAIN,\
//		    MENU_SETUP
		    
#include "ui/tui.h"

// test menu start
//#include "menu.h"

// for mq
#define MAX_TEXT 255
struct my_msg_st {
long int msg_type;
char some_text[MAX_TEXT];
};

#define MSG_QUEUE_KEY   1234

extern int MenuLinesWritten;
#define STATE_LINE      5

#include "../usbinit/disk_layout.c"

static char MKDOSFS_PATH[] = "/sbin/newfs_dos";
static char MKE2FS_PATH[] = "/sbin/mke2fs";

int child(int argc, char* argv[]) {  /* get from vold ,:) */
    // create null terminated argv_child array
    char* argv_child[argc + 1];
    memcpy(argv_child, argv, argc * sizeof(char *));
    argv_child[argc] = NULL;

    // XXX: PROTECT FROM VIKING KILLER
    if (execv(argv_child[0], argv_child)) {
        printf("logwrapper",
            "executing %s failed: %s", argv_child[0], strerror(errno));
	return -1;
    }
    return 0;
}

int usb_disk(int mountflag)
{
	char buff[MAX_TEXT];
	//char **f;
	char *fs[] = { "ext3", "ext2", "vfat", NULL };
    	int flags = /*MS_REMOUNT |*/ MS_NODEV | MS_NOEXEC | MS_NOSUID | MS_NOATIME | MS_NODIRATIME;
    	
	memset(buff, 0, MAX_TEXT);
#ifdef SOC_OMAP3
	sprintf(buff, "%sp%d", mydisk_info.device, 1); /* omap3evm alway mount first partiton */
#else
        sprintf(buff, "%sp%d", mydisk_info.device, mydisk_info.num_parts); /* alway mount last partiton */
#endif        
        
        printf("liutest: (%s, %s)\n",buff, "/sdcard");
        #if 1
	if(mountflag){
		/* mount to /sdcard */
        	sleep(1);
        		
        	
#ifdef SOC_OMAP3         /* FIXME for judge disklayout ,and number of tmp partion for install */	
		if( mount(buff, "/sdcard", fs[2], flags, NULL) ){ /* only mount vfat */
#else
                if( mount(buff, "/sdcard", fs[1], flags, NULL) ){ /* only mount ext2 */
#endif                	
            		printf("mount (%s, %s) errno: %d",buff, "/sdcard",errno);
            		return -1;
		}
	}
	else{
		if(umount("/sdcard")){
			printf("umount /sdcard fail\n");
			return -1;
		}
	}
	#endif
	
	printf("liutest: usb_disk exit (%s, %s)\n",buff, "/sdcard");
	
	return 0;
}

int check_disk_init(int alway_init_flag, int install_mode)
{
	char buff[MAX_TEXT];
	pid_t   pid;
	int i,no_init = 0;
	
	
	// for msg queue
	/*
    	struct my_msg_st msg_data;
    	int msgid;
        
        msgid = msgget((key_t)MSG_QUEUE_KEY, 0666 | IPC_CREAT);
        if (msgid == -1) {
	fprintf(stderr, "msgget failed with error: %d\n", errno);
		exit(EXIT_FAILURE);
        }
        */
        DisplayString (STATE_LINE, "disk init starting...", TEXT_JUSTIFICATION_CENTER, 0, TEXT_ATTRIBUTE_NORMAL);
        
        if(alway_init_flag){
        	no_init = 1;
        	goto init_disk;
        }
    
	for(i = 0; i<mydisk_info.num_parts; i++){
		memset(buff, 0, MAX_TEXT);
		sprintf(buff, "%sp%d", mydisk_info.device, i+1);
                printf("%s\n",buff);
		if( access(buff, R_OK) != 0 ) {
			// disk init
			no_init = 1;
			break;
		}
	}

init_disk:	
	if(no_init ){
		printf("liutest: start diskinit\n");
		if ((pid = fork()) < 0) {
           		printf("liu: diskinit fork error\n");
           		return -1;
        	} else if (pid == 0) {    /* specify pathname, specify environment */
                	if (execv("/sbin/diskinit",NULL) < 0){
                		printf("execv error");
                		return -1;
                	}
                }

        	if (waitpid(pid, NULL, 0) < 0){      /* wait diskinit exit */
             		printf("wait error");
             		return -1;
		}
		
		if(install_mode == USB_INSTALL_ARG) {
			memset(buff, 0, MAX_TEXT);
			#ifdef SOC_OMAP3
			sprintf(buff, "%sp%d", mydisk_info.device, 1);  /* FIXME */
			#else
		        sprintf(buff, "%sp%d", mydisk_info.device, mydisk_info.num_parts);
		        #endif
	       		if ((pid = fork()) < 0) {
        	   		printf("liu: format fork error\n");
           			return -1;
        		} else if (pid == 0) {    /* specify pathname, specify environment */
        			#ifdef SOC_OMAP3
        			if (mypart_info[0].type == PC_PART_TYPE_DOS) {
        			#else
		        	if (mypart_info[mydisk_info.num_parts-1].type == PC_PART_TYPE_DOS) {
		        	#endif	
		        		/*  call => mkdosfs -c 32 -n 2 ... */
        				/*char *args[6];
        				args[0] = "/sbin/newfs_msdos";
        				args[1] = "-c 32";
        				args[2] = "-n 2";
        				args[3] = "-O android";
        				args[4] = devpath;
        				args[5] = NULL;
        				*/
        				/* newfs_dos -F 16 -O install /dev/mmcblk0p1 */
        				char *args[7];
        				args[0] = MKDOSFS_PATH;
        				args[1] = "-F";
        				#ifdef SOC_OMAP3
        				if ((mypart_info[0].len_kb * 1024) <= (unsigned int) (1024*1024*1024*2))
        				#else
					if ((mypart_info[mydisk_info.num_parts-1].len_kb * 1024) <= (unsigned int) (1024*1024*1024*2))
					#endif	
        					args[2] = "16";
					else
        					args[2] = "32";

        				args[3] = "-O";
        				args[4] = "linux";       /* no lable */
        				args[5] = buff;
        				args[6] = NULL;
        				if(child(6, args)){
        					printf("dos format %s fail\n",buff);
        					return -1;
        				}
    				} else { /* use ext2 for fast */
        				char *args[7];
        				args[0] = "/sbin/mke2fs";
        				args[1] = "-b 4096";
        				args[2] = "-m 1";
        				args[3] = "-L u-tools";
        				args[4] = "-v";
        				args[5] = buff;
        				args[6] = NULL;
        				if(child(6, args)){
        					printf("format %s fail\n",buff);
        					return -1;
        				}
    				}              	
    			}
    		       	if (waitpid(pid, NULL, 0) < 0){      /* wait usb tmp disk format exit */
             			printf("wait error");
             			return -1;
			}
		}
		
		printf("liutest: start diskinit\n");
	}
	
	return 0;
}

void ActionInstaller(int argument)
{
	int msg_processed;
	pid_t fork_result;
	//char msg_data[255];

	//DisplayString (5, "test installer message", TEXT_JUSTIFICATION_CENTER, 0, TEXT_ATTRIBUTE_NORMAL);  //???
	//sleep(30);
	int running = 1;
	int msgid;
	struct my_msg_st msg_data;
	long int msg_to_receive = 0;
	int rcv_bytes = 0;
	
	int i;
	int StartingRow = MENU_STARTING_ROW;

	// clear previous menu, keeping the rest of the bootloader messages at bottom
	for (i = 0; i < (MenuLinesWritten + 1); i++)
		DisplayString (StartingRow++, "                                        ", TEXT_JUSTIFICATION_CENTER, 0, TEXT_ATTRIBUTE_NORMAL);  //???

	
	fork_result = fork();
	if (fork_result == (pid_t)-1) {
		fprintf(stderr, "Fork failure");
		exit(EXIT_FAILURE);
	}
	if (fork_result == (pid_t)0) {
		sleep(1);
		//check disk init
		if( check_disk_init(0,argument))
			printf("check_disk_init fail\n");
		else
			printf("check_disk_init Ok\n");
		
		switch (argument){
		case USB_INSTALL_ARG:
			printf("exec usb intaller\n");
			if(usb_disk(1)){  // mount usb tmp disk
				printf("mount  usb disk faile\n");
				return -1;
			}
				
			execlp("/sbin/installer", "installer", "-u", (char *)0);
			printf("exec usb intaller err\n");
			break;
		case DISK_INSTALL_ARG:
			printf("exec disk intaller\n");
			execlp("/sbin/installer", "installer", "-d", (char *)0);
			printf("exec disk intaller err\n");
			break;
		}
	}
	else {
		USER_INPUT_CODE  UserInput;
		
                printf(" intaller status ...\n");
		msgid = msgget((key_t)MSG_QUEUE_KEY, 0666 | IPC_CREAT);
		if (msgid == -1) {
			fprintf(stderr, "msgget failed with error: %d\n", errno);
			exit(EXIT_FAILURE);
		}
	
		while(running) {
			if ((rcv_bytes = msgrcv(msgid, (void *)&msg_data, 255, msg_to_receive, 0)) == -1) {
				fprintf(stderr, "msgrcv failed with error: %d\n", errno);
				exit(EXIT_FAILURE);
			}

			//if(rcv_bytes > 255)
			//	rcv_bytes = 255;
			//msg_data.some_text[rcv_bytes] = 0;
			DisplayString (STATE_LINE, msg_data.some_text, TEXT_JUSTIFICATION_CENTER, 0, TEXT_ATTRIBUTE_NORMAL);
		
			if(msg_data.msg_type == 2) {
				DisplayString (STATE_LINE+1, "please enter any key...", TEXT_JUSTIFICATION_CENTER, 0, TEXT_ATTRIBUTE_NORMAL);
				if(argument == USB_INSTALL_ARG){
					/*char *args[4];
        				args[0] = "/bin/rm";
        				args[1] = "-f";
        				args[2] = "/sdcard/installer.conf";
        				args[3] = NULL;
        				if(child(3, args)){
        					printf("\n rm conf file fail\n");
        				}
        				*/
					printf("usb installer compelet ,umount usb disk\n");
				        usb_disk(0);
				}
				

				//do{
				UserInput=GetUserInput(0);
				//	if(USER_INPUT_UP == UserInput)
				//		break;
				//}while(1);
				running = 0;
			}
		}
		
		if (msgctl(msgid, IPC_RMID, 0) == -1) {
			fprintf(stderr, "msgctl(IPC_RMID) failed\n");
			exit(EXIT_FAILURE);
		}
		
		/*if(argument == USB_INSTALL_ARG){
			printf("usb installer compelet ,umount usb disk\n");
			usb_disk(0);
		}*/
			
        }			

	return;
}



MENULINE_INFO menus[] = {
    INSTANTIATE_MENU_INFO {NULL, NULL, NO_MORE_MENUS, 1}
};
static MENUS gStartingMenu = MENU_MAIN;


//liutest
//char strtmp[512*8];

int
main(int argc, char** argv)
{
    int i;
    printf(HELLO_);
    //printf("HELLO");
    //return 0;
    
    //LOGE("test liblog loge func");
    //__android_log_print(/*int prio*/1, /*const char *tag*/"hello",  /*const char *fmt*/"test liblog loge func");
    
    if (open_framebuffer()) {
    	printf("Open framerbuffer failed");
		  goto exit_hello;
	  }

	  for (i = 0; i < NR_COLORS; i++)
		   setcolor (i, palette [i]);

	  //put_string_center (xres / 2, yres / 4, "utools diskintall ...", 1);
	  //put_string_center (xres / 2, yres / 4+20, "utools diskintall ...", 2);
	  //put_string_center (xres / 2, yres / 4+40, "utools diskintall ...", 3);
	  //put_string_center (xres / 2, yres / 4+60, "utools diskintall ...", 0);
		
    tui_init(xres,yres,0,/* font_width*/8,/* font_hight*/16,/* disp_bpp */16);
    
    // test menu			   
    setMenus(menus,(sizeof (menus) / sizeof (MENULINE_INFO)),gStartingMenu);
    GetUserActionFromMenu ((MENUS) gStartingMenu);

     
#if 0                     /* for dnw usb down load test */
     int binit=1;         /* always init */
     // test installer at usb tmp (u-boot dnw)
     if (installer(INSTALL_USB_TMP,binit))
     	printf(" liu: usb tmp installer error!\n");
     else
     	printf(" liu: usb tmp installer success!\n");
#endif
    
     
     	
			   
exit_hello:
    return 0;
}

