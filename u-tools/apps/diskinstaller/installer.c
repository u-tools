/* commands/sysloader/installer/installer.c
 *
 * Copyright 2008, The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#define LOG_TAG "installer"

#include <sys/types.h>
#include <errno.h>
#include <fcntl.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/mount.h>
#include <sys/stat.h>
#include <sys/wait.h>

#include <sys/msg.h>
#define MAX_TEXT 255

// liunote add for va_list
#include <stdarg.h>

#include <diskconfig/config_utils.h>
#include <diskconfig/log.h>

#include <diskconfig/diskconfig.h>

#include "installer.h"

#include "../usbinit/disk_layout.c"

#define MKE2FS_BIN     "/sbin/mke2fs" //"/system/bin/mke2fs"
#define E2FSCK_BIN     "/sbin/e2fsck" //"/system/bin/e2fsck"
#define TUNE2FS_BIN    "/sbin/tune2fs" //"/system/bin/tune2fs"
#define RESIZE2FS_BIN  "/sbin/resize2fs" //"/system/bin/resize2fs"
#define MKDOSFS_BIN    "/sbin/newfs_dos"
#define FSCK_MSDOS_BIN "/sbin/fsck_msdos"

#define  INSTALLER_STATE_MSG
#ifdef INSTALLER_STATE_MSG
// for mq
struct my_msg_st {
long int msg_type;
char some_text[MAX_TEXT];
};
#define MSG_QUEUE_KEY   1234
#endif


static int
validate(struct disk_info *dinfo)
{
    int fd;
    int sect_sz;
    uint64_t disk_size;
    uint64_t total_size;
    int cnt;
    struct stat stat;

    if (!dinfo)
        return -1;

    if ((fd = open(dinfo->device, O_RDWR)) < 0) {
        LOGE("Cannot open device '%s' (errno=%d)", dinfo->device, errno);
        return -1;
    }

    if (fstat(fd, &stat)) {
        LOGE("Cannot stat file '%s', errno=%d.", dinfo->device, errno);
        goto fail;
    }

    /* XXX: Some of the code below is kind of redundant and should probably
     * be refactored a little, but it will do for now. */

    /* Verify that we can operate on the device that was requested.
     * We presently only support block devices and regular file images. */
    if (S_ISBLK(stat.st_mode)) {
        /* get the sector size and make sure we agree */
        if (ioctl(fd, BLKSSZGET, &sect_sz) < 0) {
            LOGE("Cannot get sector size (errno=%d)", errno);
            goto fail;
        }

        if (!sect_sz || sect_sz != dinfo->sect_size) {
            LOGE("Device sector size is zero or sector sizes do not match!");
            goto fail;
        }

        /* allow the user override the "disk size" if they provided num_lba */
        if (!dinfo->num_lba) {
            if (ioctl(fd, BLKGETSIZE64, &disk_size) < 0) {
                LOGE("Could not get block device size (errno=%d)", errno);
                goto fail;
            }
            /* XXX: we assume that the disk has < 2^32 sectors :-) */
            dinfo->num_lba = (uint32_t)(disk_size / (uint64_t)dinfo->sect_size);
        } else
            disk_size = (uint64_t)dinfo->num_lba * (uint64_t)dinfo->sect_size;
    } else if (S_ISREG(stat.st_mode)) {
        LOGI("Requesting operation on a regular file, not block device.");
        if (!dinfo->sect_size) {
            LOGE("Sector size for regular file images cannot be zero");
            goto fail;
        }
        if (dinfo->num_lba)
            disk_size = (uint64_t)dinfo->num_lba * (uint64_t)dinfo->sect_size;
        else {
            dinfo->num_lba = (uint32_t)(stat.st_size / dinfo->sect_size);
            disk_size = (uint64_t)stat.st_size;
        }
    } else {
        LOGE("Device does not refer to a regular file or a block device!");
        goto fail;
    }

#if 0
    LOGV("Device/file %s: size=%llu bytes, num_lba=%u, sect_size=%d",
         dinfo->device, disk_size, dinfo->num_lba, dinfo->sect_size);
#endif

    /* since this is our offset into the disk, we start off with that as
     * our size of needed partitions */
    total_size = dinfo->skip_lba * dinfo->sect_size;

    /* add up all the partition sizes and make sure it fits */
    for (cnt = 0; cnt < dinfo->num_parts; ++cnt) {
    	//LOGE("liu:validate cnt=%d,dinfo->num_parts=%d",cnt,dinfo->num_parts);
        struct part_info *part = &dinfo->part_lst[cnt];
        if (part->len_kb != (uint32_t)-1) {
            total_size += part->len_kb * 1024;
        } else if (part->len_kb == 0) {
            LOGE("Zero-size partition '%s' is invalid.", part->name);
            goto fail;
        } else {
            /* the partition requests the rest of the disk. */
            if (cnt + 1 != dinfo->num_parts) {
                LOGE("Only the last partition in the list can request to fill "
                     "the rest of disk.");
                goto fail;
            }
        }

        if ((part->type != PC_PART_TYPE_LINUX) && (part->type != PC_PART_TYPE_DOS)){
            LOGE("Unknown partition type (0x%x) encountered for partition "
                 "'%s'\n", part->type, part->name);
            goto fail;
        }
    }

    /* only matters for disks, not files */
    if (S_ISBLK(stat.st_mode) && total_size > disk_size) {
        LOGE("Total requested size of partitions (%llu) is greater than disk "
             "size (%llu).", total_size, disk_size);
        goto fail;
    }

    return fd;

fail:
    close(fd);
    return -1;
}

static inline uint32_t
kb_to_lba(uint32_t len_kb, uint32_t sect_size)
{
    uint64_t lba;

    lba = (uint64_t)len_kb * 1024;
    /* bump it up to the next LBA boundary just in case  */
    lba = (lba + (uint64_t)sect_size - 1) & ~((uint64_t)sect_size - 1);
    lba /= (uint64_t)sect_size;
    if (lba >= 0xffffffffULL)
        LOGE("Error converting kb -> lba. 32bit overflow, expect weirdness");
    return (uint32_t)(lba & 0xffffffffULL);
}

static int
fix_pri_pentry(struct disk_info *dinfo, struct part_info *pinfo, int pnum,
              uint32_t *lba)
{
    if (pnum >= PC_NUM_BOOT_RECORD_PARTS) {
        LOGE("Maximum number of primary partition exceeded.");
        return 0;/*NULL*/;
    }

    /* need a standard primary partition entry */
    if (pinfo) {
        /* need this to be 64 bit in case len_kb is large */
        uint64_t len_lba; 

        if (pinfo->len_kb != (uint32_t)-1) {
            /* bump it up to the next LBA boundary just in case */
            len_lba = ((uint64_t)pinfo->len_kb * 1024);
            len_lba += ((uint64_t)dinfo->sect_size - 1);
            len_lba &= ~((uint64_t)dinfo->sect_size - 1);
            len_lba /= (uint64_t)dinfo->sect_size;
        } else {
            /* make it fill the rest of disk */
            len_lba = dinfo->num_lba - *lba;
        }

        pinfo->start_lba = *lba;
        *lba += (uint32_t)len_lba;
    }
    return 1;
}

static int 
fix_ext_pentry(struct disk_info *dinfo, struct part_info *pinfo, uint32_t *lba,
              uint32_t ext_lba, struct part_info *pnext)
{
    uint32_t len; /* in lba units */

    (*lba)++;

    if (pinfo->len_kb != (uint32_t)-1)
        len = kb_to_lba(pinfo->len_kb, dinfo->sect_size);
    else {
        if (pnext) {
            LOGE("Only the last partition can be specified to fill the disk "
                 "(name = '%s')", pinfo->name);
            return 0;//goto fail;
        }
        len = dinfo->num_lba - *lba;
        /* update the pinfo structure to reflect the new size, for
         * bookkeeping */
        pinfo->len_kb =
            (uint32_t)(((uint64_t)len * (uint64_t)dinfo->sect_size) /
                       ((uint64_t)1024));
    }

    pinfo->start_lba = *lba;
    *lba += len;

    /* If this is not the last partition, we have to create a link to the
     * next extended partition.
     *
     * Otherwise, there's nothing to do since the "pointer entry" is
     * already zero-filled.
     */
    if (pnext) {
        /* The start lba for next partition is an offset from the beginning
         * of the top-level extended partition */
        uint32_t next_start_lba = *lba - ext_lba;
        uint32_t next_len_lba;
        if (pnext->len_kb != (uint32_t)-1)
            next_len_lba = 1 + kb_to_lba(pnext->len_kb, dinfo->sect_size);
        else
            next_len_lba = dinfo->num_lba - *lba;
    }
    return 1;
}

static int
fix_disklayout(struct disk_info *dinfo)
{
    struct part_info *pinfo;
    uint32_t cur_lba = dinfo->skip_lba;
    uint32_t ext_lba = 0;
    int cnt = 0;
    int extended = 0;

    if (!dinfo->part_lst)
        return 0;//-1;

    for (cnt = 0; cnt < dinfo->num_parts; ++cnt) {
        pinfo = &dinfo->part_lst[cnt];

        /* Should we create an extedned partition? */
        if (cnt == (PC_NUM_BOOT_RECORD_PARTS /*- 1*/)) {  // liu re -1
            if (cnt + 1 < dinfo->num_parts) {
                extended = 1;
                ext_lba = cur_lba;
                fix_pri_pentry(dinfo, NULL, cnt, &cur_lba);
            }
        }

        /* if extended, need 1 lba for ebr */
        if ((cur_lba + extended) >= dinfo->num_lba)
            goto nospace;
        else if (pinfo->len_kb != (uint32_t)-1) {
            uint32_t sz_lba = (pinfo->len_kb / dinfo->sect_size) * 1024;
            if ((cur_lba + sz_lba + extended) > dinfo->num_lba)
                goto nospace;
        }

        if (!extended)
            fix_pri_pentry(dinfo, pinfo, cnt, &cur_lba);
        else {
            struct part_info *pnext;
            pnext = cnt + 1 < dinfo->num_parts ? &dinfo->part_lst[cnt+1] : NULL;
            fix_ext_pentry(dinfo, pinfo, &cur_lba, ext_lba, pnext);
        }
    }

    /* fill in the rest of the MBR with empty parts (if needed). */
    for (; cnt < PC_NUM_BOOT_RECORD_PARTS; ++cnt) {
        struct part_info blank;
        cur_lba = 0;
        memset(&blank, 0, sizeof(struct part_info));
        fix_pri_pentry(dinfo, &blank, cnt, &cur_lba);
    }

    return 0;

nospace:
    LOGE("Not enough space to add parttion '%s'.", pinfo->name);

//fail:
    return -1;
}

static int
usage(void)
{
#if 0	
    fprintf(stderr, "Usage: %s\n", LOG_TAG);
    fprintf(stderr, "\t-c <path> - Path to installer conf file "
                    "(/system/etc/installer.conf)\n");
    fprintf(stderr, "\t-l <path> - Path to device disk layout conf file "
                    "(/system/etc/disk_layout.conf)\n");
    fprintf(stderr, "\t-h        - This help message\n");
    fprintf(stderr, "\t-d        - Dump the compiled in partition info.\n");
    fprintf(stderr, "\t-p <path> - Path to device that should be mounted"
                    " to /data.\n");
    fprintf(stderr, "\t-t        - Test mode. Don't write anything to disk.\n");
#else
    fprintf(stderr, "Usage: %s\n", LOG_TAG);
    fprintf(stderr, "\t-h        - This help message\n");
    fprintf(stderr, "\t-d        - Disk installer.\n");
    fprintf(stderr, "\t-u <path> - Usb installer0721.\n");
#endif    
    return 1;
}

static cnode *
read_conf_file(const char *fn)
{
    cnode *root = config_node("", "");
    config_load_file(root, fn);

    if (root->first_child == NULL) {
        LOGE("Could not read config file %s", fn);
        return NULL;
    }

    return root;
}

static int
exec_cmd(const char *cmd, ...) /* const char *arg, ...) */
{
    va_list ap;
    int size = 0;
    char *str;
    char *outbuf;
    int rv;

    /* compute the size for the command buffer */
    size = strlen(cmd) + 1;
    va_start(ap, cmd);
    while ((str = va_arg(ap, char *))) {
        size += strlen(str) + 1;  /* need room for the space separator */
    }
    va_end(ap);

    if (!(outbuf = malloc(size + 1))) {
        LOGE("Can't allocate memory to exec cmd");
        return -1;
    }

    /* this is a bit inefficient, but is trivial, and works */
    strcpy(outbuf, cmd);
    va_start(ap, cmd);
    while ((str = va_arg(ap, char *))) {
        strcat(outbuf, " ");
        strcat(outbuf, str);
    }
    va_end(ap);

    LOGI("Executing: %s", outbuf);
    rv = system(outbuf);
    free(outbuf);
    if (rv < 0) {
        LOGI("Error while trying to execute '%s'", cmd);
        return -1;
    }
    rv = WEXITSTATUS(rv);
    LOGI("Done executing %s (%d)", outbuf, rv);
    return rv;
}


static int
do_fsck(const char *dst, int force)
{
    int rv;
    const char *opts = force ? "-fy" : "-y";


    LOGI("Running e2fsck... (force=%d) This MAY take a while.", force);
    if ((rv = exec_cmd(E2FSCK_BIN, "-C 0", opts, dst, NULL)) < 0)
        return 1;
    if (rv >= 4) {
        LOGE("Error while running e2fsck: %d", rv);
        return 1;
    }
    sync();
    LOGI("e2fsck succeeded (exit code: %d)", rv);

    return 0;
}

static int
process_ext2_image(const char *dst, const char *src, uint32_t flags, int test)
{
    int rv;

    /* First, write the image to disk. */
    if (write_raw_image(dst, src, 0, test))
        return 1;

    if (test)
        return 0;

    /* Next, let's e2fsck the fs to make sure it got written ok, and
     * everything is peachy */
    if (do_fsck(dst, 1))
        return 1;

    /* set the mount count to 1 so that 1st mount on boot doesn't complain */
    if ((rv = exec_cmd(TUNE2FS_BIN, "-C", "1", dst, NULL)) < 0)
        return 1;
    if (rv) {
        LOGE("Error while running tune2fs: %d", rv);
        return 1;
    }

    /* If the user requested that we resize, let's do it now */
    if (flags & INSTALL_FLAG_RESIZE) {
        if ((rv = exec_cmd(RESIZE2FS_BIN, "-F", dst, NULL)) < 0)
            return 1;
        if (rv) {
            LOGE("Error while running resize2fs: %d", rv);
            return 1;
        }
        sync();
        if (do_fsck(dst, 0))
            return 1;
    }

    /* make this an ext3 fs? */
    if (flags & INSTALL_FLAG_ADDJOURNAL) {
        if ((rv = exec_cmd(TUNE2FS_BIN, "-j", dst, NULL)) < 0)
            return 1;
        if (rv) {
            LOGE("Error while running tune2fs: %d", rv);
            return 1;
        }
        sync();
        if (do_fsck(dst, 0))
            return 1;
    }
    
    if (flags & INSTALL_FLAG_ADDEXT4) {
        if ((rv = exec_cmd(TUNE2FS_BIN, "-O","extents,uninit_bg,dir_index,has_journal", dst, NULL)) < 0)
            return 1;
        if (rv) {
            LOGE("Error while running tune2fs: %d", rv);
            return 1;
        }
        sync();
        if (do_fsck(dst, 0))
            return 1;
    }

    

    return 0;
}

/* FIXME for tar dest mount dir,and data_fstype */
static int
process_tar_image(const char *dst, const char *src, uint32_t flags, int test)
{
    int rv;

    if (test)
        return 0;

    if (mount(dst, "/mnt", "ext4", /*MS_SYNCHRONOUS*/MS_NODEV | MS_NOEXEC | MS_NOSUID | MS_NOATIME | MS_NODIRATIME, NULL)) {
          LOGE("Could not mount %s on %s as %s", dst, "/mnt",
                 "ext4");
            return 1;
    }


    /* FIXME only bz2 ? */
    /* /sbin/tar --use-compress-program /sbin/bzip2 -xvf  rootfs.tar.bz2  -C /mnt */
    if (flags == INSTALL_IMAGE_TARBZ) {
        if ((rv = exec_cmd("/sbin/tar", "--use-compress-program","/sbin/bzip2","-xvf", src,"-C","/mnt", NULL)) < 0)
            return 1;
        if (rv) {
            LOGE("tar fail: %d", rv);
            return 1;
        }
        sync();
        
        if (umount("/mnt"/*, 0*/))
            return 1;
    }
    
    
    return 0;
}

/* TODO: PLEASE break up this function into several functions that just
 * do what they need with the image node. Many of them will end up
 * looking at same strings, but it will be sooo much cleaner */
static int
process_image_node(cnode *img, struct disk_info *dinfo, int test)
{
    struct part_info *pinfo = NULL;
    loff_t offset = (loff_t)-1;
    const char *filename = NULL;
    char *dest_part = NULL;
    const char *tmp;
    uint32_t flags = 0;
    uint8_t type = 0;
    int rv;
    int func_ret = 1;

    filename = config_str(img, "filename", NULL);

    /* process the 'offset' image parameter */
    if ((tmp = config_str(img, "offset", NULL)) != NULL)
        offset = strtoull(tmp, NULL, 0);

    /* process the 'partition' image parameter */
    if ((tmp = config_str(img, "partition", NULL)) != NULL) {
        if (offset != (loff_t)-1) {
            LOGE("Cannot specify the partition name AND an offset for %s",
                 img->name);
            goto fail;
        }

        if (!(pinfo = find_part(dinfo, tmp))) {
            LOGE("Cannot find partition %s while processing %s",
                 tmp, img->name);
            goto fail;
        }

        if (!(dest_part = find_part_device(dinfo, pinfo->name))) {
            LOGE("Could not get the device name for partition %s while"
                 " processing image %s", pinfo->name, img->name);
            goto fail;
        }
        offset = pinfo->start_lba * dinfo->sect_size;
    }

    /* process the 'mkfs' parameter */
    if ((tmp = config_str(img, "mkfs", NULL)) != NULL) {
        char *journal_opts;
        char vol_lbl[16]; /* ext2/3 has a 16-char volume label */

        if (!pinfo) {
            LOGE("Target partition required for mkfs for '%s'", img->name);
            goto fail;
        } /*else if (filename) {
            LOGE("Providing filename and mkfs parameters is meaningless");
            goto fail;
        }*/

        if (!strcmp(tmp, "ext4"))
            journal_opts = "ext4";      // for ext4
        else if (!strcmp(tmp, "ext2"))
            journal_opts = "";
        else if (!strcmp(tmp, "ext3"))
            journal_opts = "-j";
        else if (!strcmp(tmp, "vfat"))
            journal_opts = "32";        // FIXME for judge <=2G use 16(fat16)
        else {
            LOGE("Unknown filesystem type for mkfs: %s", tmp);
            goto fail;
        }

        /* put the partition name as the volume label */
        strncpy(vol_lbl, pinfo->name, sizeof(vol_lbl));

        /* since everything checked out, lets make the fs, and return since
         * we don't need to do anything else */
        if (!strcmp(tmp, "vfat")) /* newfs_dos -F 32 -O install /dev/mmcblk0p3 */
        rv = exec_cmd(MKDOSFS_BIN, "-F",journal_opts, "-O", vol_lbl,dest_part, NULL);
        	
        else if (!strcmp(tmp, "ext4"))
        rv = exec_cmd(MKE2FS_BIN, "-L", vol_lbl, "-t",journal_opts, dest_part, NULL);
        else	
        rv = exec_cmd(MKE2FS_BIN, "-L", vol_lbl, journal_opts, dest_part, NULL);
        if (rv < 0)
            goto fail;
        else if (rv > 0) {
            LOGE("Error while running mke2fs: %d", rv);
            goto fail;
        }
        sync();
        if (!strcmp(tmp, "vfat")){ /* FIXME for uni do_fsck (and tar to vfat ?) */
        	rv = exec_cmd(FSCK_MSDOS_BIN, "-p","-f",dest_part, NULL);
        	if (rv < 0)
            		goto fail;
        }
        else{	
        if (do_fsck(dest_part, 0))
            goto fail;
    	}
        if (!filename)     
        	goto done;
    }

    /* since we didn't mkfs above, all the rest of the options assume
     * there's a filename involved */
    if (!filename) {
        LOGE("Filename is required for image %s", img->name);
        goto fail;
    }

    /* process the 'flags' image parameter */
    if ((tmp = config_str(img, "flags", NULL)) != NULL) {
        char *flagstr, *flagstr_orig;

        if (!(flagstr = flagstr_orig = strdup(tmp))) {
            LOGE("Cannot allocate memory for dup'd flags string");
            goto fail;
        }
        while ((tmp = strsep(&flagstr, ","))) {
            if (!strcmp(tmp, "resize"))
                flags |= INSTALL_FLAG_RESIZE;
            else if (!strcmp(tmp, "addjournal"))
                flags |= INSTALL_FLAG_ADDJOURNAL;
            else {
                LOGE("Unknown flag '%s' for image %s", tmp, img->name);
                free(flagstr_orig);
                goto fail;
            }
        }
        free(flagstr_orig);
    }

    /* process the 'type' image parameter */
    if (!(tmp = config_str(img, "type", NULL))) {
        LOGE("Type is required for image %s", img->name);
        goto fail;
    } else if (!strcmp(tmp, "raw")) {
        type = INSTALL_IMAGE_RAW;
    } else if (!strcmp(tmp, "ext2")) {
        type = INSTALL_IMAGE_EXT2;
    } else if (!strcmp(tmp, "ext3")) {
        type = INSTALL_IMAGE_EXT3;
    } else if (!strcmp(tmp, "ext4")) {
        type = INSTALL_IMAGE_EXT4;
    } else if (!strcmp(tmp, "tarbz")) {
        type = INSTALL_IMAGE_TARBZ;
    } else {
        LOGE("Unknown image type '%s' for image %s", tmp, img->name);
        goto fail;
    }

    /* at this point we MUST either have a partition in 'pinfo' or a raw
     * 'offset', otherwise quit */
    if (!pinfo && (offset == (loff_t)-1)) {
        LOGE("Offset to write into the disk is unknown for %s", img->name);
        goto fail;
    }

    if (!pinfo && (type != INSTALL_IMAGE_RAW)) {
        LOGE("Only raw images can specify direct offset on the disk. Please"
             " specify the target partition name instead. (%s)", img->name);
        goto fail;
    }

    switch(type) {
        case INSTALL_IMAGE_RAW:
            if (write_raw_image(dinfo->device, filename, offset, test))
                goto fail;
            break;

        case INSTALL_IMAGE_EXT3:
            /* makes the error checking in the imager function easier */
            if (flags & INSTALL_FLAG_ADDJOURNAL) {
                LOGW("addjournal flag is meaningless for ext3 images");
                flags &= ~INSTALL_FLAG_ADDJOURNAL;
            }
            /* ...fall through... */

        case INSTALL_IMAGE_EXT4:
        	flags |= INSTALL_FLAG_ADDEXT4;
            /* fallthru */

        case INSTALL_IMAGE_EXT2:
            if (process_ext2_image(dest_part, filename, flags, test))
                goto fail;
            break;
            
        case INSTALL_IMAGE_TARBZ:
            flags = INSTALL_IMAGE_TARBZ;	
            if(process_tar_image(dest_part, filename, flags, test))		    
            	goto fail;
            break;	

        default:
            LOGE("Unknown image type: %d", type);
            goto fail;
    }

done:
    func_ret = 0;

fail:
    if (dest_part)
        free(dest_part);
    return func_ret;
}


char * install_conf_filename[2] = {DISK_INSTALL_CONF_FILE,USB_INSTALL_CONF_FILE};
char * install_data_dirname[2]  = {DISK_INSTALL_DATA_DIR,USB_INSTALL_DATA_DIR};
char * install_data_devname[2]  = {DISK_INSTALL_DATA_DEV,USB_INSTALL_DATA_DEV};
char * install_data_typename[2] = {DISK_INSTALL_DATA_TYPE,USB_INSTALL_DATA_TYPE};



main(int argc, char *argv[])
{
    char *disk_conf_file = "/etc/disk_layout.conf";     // liunote always this dir
    char *inst_conf_file; //= "/etc/installer.conf";
    char *inst_data_dir; //= "/data";
    char *inst_data_dev; //= "/dev/mmcblk1p1"; //NULL;    // liunote  ram is ramdisk tmp dir
    char *data_fstype; //= "vfat"; //"ext2";              // liunote
    cnode *config;
    cnode *images;
    cnode *img;
    int cnt = 0;
    struct disk_info *device_disk_info;
    int dump = 0;
    int test = 0;
    int x;

    
    int install_mode = 0;
    
#ifdef INSTALLER_STATE_MSG
    // for msg queue
    struct my_msg_st msg_data;
    int msgid;
    printf("liutest installer runing\n");
    
    msgid = msgget((key_t)MSG_QUEUE_KEY, 0666 | IPC_CREAT);
    if (msgid == -1) {
	fprintf(stderr, "msgget failed with error: %d\n", errno);
		exit(EXIT_FAILURE);
    }
    
#endif
    

#if 0
    while ((x = getopt (argc, argv, "thdc:l:p:")) != EOF) {
        switch (x) {
            case 'h':
                return usage();
            case 'c':
                inst_conf_file = optarg;
                break;
            case 'l':
                disk_conf_file = optarg;
                break;
            case 't':
                test = 1;
                break;
            case 'p':
                inst_data_dev = optarg;
                break;
            case 'd':
                dump = 1;
                break;
            default:
                fprintf(stderr, "Unknown argument: %c\n", (char)optopt);
                return usage();
        }
    }
#else
    while ((x = getopt (argc, argv, "hdu")) != EOF) {
        switch (x) {
            case 'h':
#if 0            	
            	msgid = msgget((key_t)MSG_QUEUE_KEY, 0666 | IPC_CREAT);
    		if (msgid == -1) {
			fprintf(stderr, "msgget failed with error: %d\n", errno);
			exit(EXIT_FAILURE);
    		}
        	msg_data.msg_type = 1;
		strcpy(msg_data.some_text, "liutest intaller runing!\n");
		if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
			fprintf(stderr, "msgsnd failed\n");
			exit(EXIT_FAILURE);
		}
		
		sleep(30);
        	msg_data.msg_type = 2;  //end
		strcpy(msg_data.some_text, "liutest intaller end!\n");
		if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
			fprintf(stderr, "msgsnd failed\n");
			exit(EXIT_FAILURE);
		}

            	printf("liutest stdout \n");
#endif            	

                return usage();
            case 'd':
                install_mode = DISK_INSTALL;
                #ifdef INSTALLER_STATE_MSG
                	msg_data.msg_type = 1;
                	printf("sdcard install starting...\n");
			strcpy(msg_data.some_text, "sdcard install starting...!");
			if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
				fprintf(stderr, "msgsnd failed\n");
				return 1;
				//exit(EXIT_FAILURE);
			}
                #endif
                break;
            case 'u':
                install_mode = USB_INSTALL;
                fprintf(stderr, "liu:-u <path> - Usb installer0721.\n");
                LOGE("liu:-u <path> - Usb installer0721");
                #ifdef INSTALLER_STATE_MSG
                	msg_data.msg_type = 1;
                	printf("usb install starting...\n");
			strcpy(msg_data.some_text, "usb install starting!");
			if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
				fprintf(stderr, "msgsnd failed\n");
				return 1;
				//exit(EXIT_FAILURE);
			}
                #endif
                break;
            default:
                fprintf(stderr, "Unknown argument: %c\n", (char)optopt);
                return usage();
        }
    }
#endif    

     
    inst_conf_file = install_conf_filename[install_mode];
    inst_data_dir  = install_data_dirname[install_mode];
    inst_data_dev  = install_data_devname[install_mode];
    data_fstype    = install_data_typename[install_mode];

    
    /* liunote add for usb */
    if (install_mode == DISK_INSTALL){
    	
	#ifdef INSTALLER_STATE_MSG
               	msg_data.msg_type = 1;
		strcpy(msg_data.some_text, "please insert sdcard ,then waiting read file!");
		if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
			fprintf(stderr, "msgsnd failed\n");
			//exit(EXIT_FAILURE);
		}
	#endif

    /* If the user asked us to wait for data device, wait for it to appear,
     * and then mount it onto /data */
    if (inst_data_dev && !dump) {
        struct stat filestat;

        LOGI("Waiting for device: %s", inst_data_dev);
        while (stat(inst_data_dev, &filestat))
            sleep(1);
        LOGI("Device %s ready", inst_data_dev);
        if (mount(inst_data_dev, inst_data_dir, data_fstype, MS_RDONLY, NULL)) {
            LOGE("Could not mount %s on %s as %s", inst_data_dev, inst_data_dir,
                 data_fstype);
            return 1;
        }
    }
    
    }
    else if (install_mode == USB_INSTALL) {
#if 1   	
    	pid_t   pid;
        #ifdef INSTALLER_STATE_MSG
               	msg_data.msg_type = 1;
		strcpy(msg_data.some_text, "please insert usb and push file ,then waiting trans file!");
		if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
			fprintf(stderr, "msgsnd failed\n");
			//exit(EXIT_FAILURE);
		}
        #endif
        
        //if(usb_disk(1)){
        //	printf("mount usb tmp disk fail\n");
        //	exit(EXIT_FAILURE); 
        //}
                    	
    	/* wait file usb donwload to tmp ,complete */
        if ((pid = fork()) < 0) {
           printf("liu: fork error\n");
        } else if (pid == 0) {    /* specify pathname, specify environment */
             if (execv("/sbin/adbd",NULL) < 0)
                printf("execv error");
        }

        if (waitpid(pid, NULL, 0) < 0)      /* wait adbd exit */
             printf("wait error");
#endif             
    }

        #ifdef INSTALLER_STATE_MSG
               	msg_data.msg_type = 1;
		strcpy(msg_data.some_text, "please waiting for installer ......!");
		if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
			fprintf(stderr, "msgsnd failed\n");
			//exit(EXIT_FAILURE);
		}
        #endif


if (install_mode != USB_INSTALL) {
    /* Read and process the disk configuration */
    if (!(device_disk_info = load_diskconfig(disk_conf_file, NULL))) {
        LOGE("Errors encountered while loading disk conf file %s",
             disk_conf_file);
        return 1;
    }

    if (process_disk_config(device_disk_info)) {
        LOGE("Errors encountered while processing disk config from %s",
             disk_conf_file);
        return 1;
    }
}
else{
	int fd = -1;
	LOGE("liu:start validate disk info");
	mydisk_info.part_lst = &mypart_info;
	device_disk_info = &mydisk_info;
	if ((fd = validate(device_disk_info)) < 0){
	    LOGE("liu:Invalid disk info");
            return 1;
        }

        close(fd);
        if(fix_disklayout(device_disk_info))
        	LOGE("liu:fix disk info err");
        	
}

    /* Was all of this for educational purposes? If so, quit. */
    if (dump) {
        dump_disk_config(device_disk_info);
        return 0;
    }


    /* This doesnt do anything but load the config file */
    if (!(config = read_conf_file(inst_conf_file)))
//#ifdef SOC_OMAP3
    {
    		#ifdef INSTALLER_STATE_MSG
               	msg_data.msg_type = 2;
		strcpy(msg_data.some_text, "installer end!");
		if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
			fprintf(stderr, "msgsnd failed\n");
			//exit(EXIT_FAILURE);
		}
	#endif   
        return 0;
    }
//#else
//        return 1;
//#endif        

if (install_mode != USB_INSTALL) {
    /* First, partition the drive */
    if (apply_disk_config(device_disk_info, test))
        return 1;
}

    /* Now process the installer config file and write the images to disk */
    if (!(images = config_find(config, "images"))) {
        LOGE("Invalid configuration file %s. Missing 'images' section",
             inst_conf_file);
        return 1;
    }
    
    printf("liutest delay 5s\n");
    sleep(5);   // liunote  will modi to sync
    printf("liutest process_image_node\n");
    

    for (img = images->first_child; img; img = img->next) {
        if (process_image_node(img, device_disk_info, test))
            return 1;
        ++cnt;
    }

if (install_mode != USB_INSTALL) {
    /*
     * We have to do the apply() twice. We must do it once before the image
     * writes to layout the disk partitions so that we can write images to
     * them. We then do the apply() again in case one of the images
     * replaced the MBR with a new bootloader, and thus messed with
     * partition table.
     */
    if (apply_disk_config(device_disk_info, test))
        return 1;
}

	#ifdef INSTALLER_STATE_MSG
               	msg_data.msg_type = 2;
		strcpy(msg_data.some_text, "installer end!");
		if (msgsnd(msgid, (void *)&msg_data, MAX_TEXT, 0) == -1) {
			fprintf(stderr, "msgsnd failed\n");
			//exit(EXIT_FAILURE);
		}
	#endif


    LOGI("Done processing installer config. Configured %d images", cnt);
    return 0;
}
