from machines import Machine
from toolchains import gnu_arm_toolchain

############################################################################
# arm based machines
############################################################################
class arm(Machine):
    wordsize = 32
    arch = "arm"
    endian = "little"
    default_toolchain = gnu_arm_toolchain
    rvct_c_flags = []
    ads_c_flags = []
    arch_version_no_kidding = None

# ARM CPU Classes

class armv4(arm):
    #cpp_defines = [("ARM_PID_RELOC", 1)]
    #pid_reloc = True
    # FIXME: This is required because we don't have separate arch
    # directories for arm v4 and arm v5.
    # No Kidding! Scornful acknowledgment of the obvious.
    arch_version_no_kidding = 4

class armv5(arm):
    #cpp_defines = [("ARM_PID_RELOC", 1), ("ARM_SHARED_DOMAINS", 1)]
    #pid_reloc = True
    shared_domains = True

class armv4t(armv4):
    c_flags = armv4.c_flags + ["-march=armv4t"]
    cpp_defines = armv4.cpp_defines + [("__ARMv__", 4), "__ARMv4T__"]
    arch_version = 5
    # See comment above.
    arch_version_no_kidding = 4

class armv5te(armv5):
    c_flags = armv5.c_flags + ["-march=armv5te"]
    cpp_defines = armv5.cpp_defines + [("__ARMv__", 5), "__ARMv5TE__"]
    arch_version = 5

class armv5tej(armv5):
    c_flags = armv5.c_flags + ["-march=armv5te"]
    cpp_defines = armv5.cpp_defines + [("__ARMv__", 5), "__ARMv5TEJ__"]
    arch_version = 5


class xscale(armv5te):
    c_flags = armv5te.c_flags + ["-mtune=xscale"]
    rvct_c_flags = armv5te.rvct_c_flags + ["--cpu", "XScale"]
    ads_c_flags = armv5te.ads_c_flags + ["-cpu", "XScale"]
    cpu = "xscale"

class armv6(arm):
    c_flags = arm.c_flags + ["-march=armv5te"]

class armv6j(armv6):
    c_flags = armv6.c_flags + ["-march=armv5te"]
    #c_flags = armv6.c_flags + ["-march=armv6"]
    cpp_defines = armv6.cpp_defines + [("__ARMv__", 6), "__ARMv5TE__"]
    arch_version = 6

class arm920t(armv4t):
    c_flags = armv4t.c_flags + ["-mtune=arm920t"]
    rvct_c_flags = armv4t.rvct_c_flags + ["--cpu", "arm920t"]
    ads_c_flags = armv4t.ads_c_flags + ["-cpu", "arm920t"]
    ads_as_flags = armv4t.ads_c_flags + ["-cpu", "arm920t"]
    cpu = "arm920t"

# with floating point
class armv6jf(armv6):
    c_flags = armv6.c_flags + ["-march=armv5te"]
    cpp_defines = armv6.cpp_defines + [("__ARMv__", 6), "__ARMv5TE__"]
    arch_version = 6

class arm1136js(armv6j):
    c_flags = armv6j.c_flags + ["-Wa,-mcpu=arm1136"]
    #c_flags = armv6j.c_flags + ["-Wa,-march=armv6"]
    #c_flags = armv6j.c_flags + ["-msoft-float"]+["-mtune=arm1136j-s"] +["-Wa,-march=armv6"]+["-mabi=apcs-gnu"]
    #as_flags = ["-msoft-float"]+["-march=armv6"]+["-mtune=arm1136j-s"] +["-Wa,-march=armv6"]
    #+ ["-mabi=apcs-gnu"]+["-mno-thumb-interwork"]
    #+["-mthumb"]+ ["-Wa,-mimplicit-it=thumb"]+["-Wa,-mthumb"]
    #["-Wa,-mcpu=arm1136"]
    cpu = "arm1136js"

class arm1176jzs(armv6jf):
    cpu = "arm1176jzs"


class arm926ejs(armv5tej):
    # This breaks EABI builds because gcc 4.2.4 doesn't like this flag
    #c_flags = armv5tej.c_flags + ["-mtune=arm926ejs"]
    rvct_c_flags = armv5tej.rvct_c_flags + ["--cpu", "arm926ej-s"]
    ads_c_flags = armv5tej.ads_c_flags + ["-cpu", "arm926ej-s"]
    ads_as_flags = armv5tej.ads_c_flags + ["-cpu", "arm926ej-s"]
    cpu = "arm926ejs"

class armv7a(arm):
    # build linux -mabi=aapcs-linux -mno-thumb-interwork
    c_flags = arm.c_flags + ["-Wa,-march=armv7-a"]
    arch_version = 7


class arm_cortexa8(armv7a):
    c_flags = armv7a.c_flags + ["-msoft-float"]+["-mtune=cortex-a8"]





##################################################################################################
# define build.py MACHINE 's name  (not virtual)
##################################################################################################
class s5pc100(arm_cortexa8):  #(arm_cortexa8)  cross4.2.2 not support cortex-a8 ???
    platform = "s5pc100"
    virtual = False           # liunote must set not virtual for build.py MACHINE=xxx       
    c_flags = ["-Wa,-march=armv5t"]   # arm_cortexa8.c_flags +
    cpu = "s5pc100"

""" for android build 
#    /* define __ARM_HAVE_5TE if we have the ARMv5TE instructions */
#if __ARM_ARCH__ > 5
#  define  __ARM_HAVE_5TE  1
#elif __ARM_ARCH__ == 5
#  if defined __ARM_ARCH_5TE__ || defined __ARM_ARCH_5TEJ__
#    define __ARM_HAVE_5TE  1
#  endif
#endif

    cpp_defines =  [("__ARM_HAVE_5TE", 1)]

# /* instructions introduced in ARMv5 */
#if __ARM_ARCH__ >= 5
#  define  __ARM_HAVE_BLX  1
#  define  __ARM_HAVE_CLZ  1
#  define  __ARM_HAVE_LDC2 1
#  define  __ARM_HAVE_MCR2 1
#  define  __ARM_HAVE_MRC2 1
#  define  __ARM_HAVE_STC2 1
#endif
    
    # liunote now no use android build
    #cpp_defines += [("__ARM_HAVE_BLX", 1),("__ARM_HAVE_CLZ", 1),("__ARM_HAVE_LDC2", 1),("__ARM_HAVE_MCR2", 1),("__ARM_HAVE_MRC2", 1),("__ARM_HAVE_STC2", 1)]

#/* ARMv5TE introduces a few instructions */
#if __ARM_HAVE_5TE
#  define  __ARM_HAVE_PLD   1
#  define  __ARM_HAVE_MCRR  1
#  define  __ARM_HAVE_MRRC  1
#endif
   
    cpp_defines += [("__ARM_HAVE_PLD", 1),("__ARM_HAVE_MCRR", 1),("__ARM_HAVE_MRRC", 1)]

# /* define __ARM_HAVE_HALFWORD_MULTIPLY when half-word multiply instructions
# * this means variants of: smul, smulw, smla, smlaw, smlal
# */
#if __ARM_HAVE_5TE
#  define  __ARM_HAVE_HALFWORD_MULTIPLY  1
#endif
   
    cpp_defines += [("__ARM_HAVE_PLD", 1)]

# /* define __ARM_HAVE_PAIR_LOAD_STORE when 64-bit memory loads and stored
# * into/from a pair of 32-bit registers is supported throuhg 'ldrd' and 'strd'
# */
#if __ARM_HAVE_5TE
#  define  __ARM_HAVE_PAIR_LOAD_STORE 1
#endif
   
    cpp_defines += [("__ARM_HAVE_PAIR_LOAD_STORE", 1)]

# /* define __ARM_HAVE_SATURATED_ARITHMETIC is you have the saturated integer
# * arithmetic instructions: qdd, qdadd, qsub, qdsub
# */
#if __ARM_HAVE_5TE
#  define  __ARM_HAVE_SATURATED_ARITHMETIC 1
#endif

    cpp_defines += [("__ARM_HAVE_SATURATED_ARITHMETIC", 1)]

# /* define __ARM_HAVE_PC_INTERWORK when a direct assignment to the
# * pc register will switch into thumb/ARM mode depending on bit 0
# * of the new instruction address. Before ARMv5, this was not the
# * case, and you have to write:
# *
# *     mov  r0, [<some address>]
# *     bx   r0
# *
# * instead of:
# *
# *     ldr  pc, [<some address>]
# *
# * note that this affects any instruction that explicitely changes the
# * value of the pc register, including ldm { ...,pc } or 'add pc, #offset'
# */
#if __ARM_ARCH__ >= 5
#  define __ARM_HAVE_PC_INTERWORK
#endif

    cpp_defines += ["__ARM_HAVE_PC_INTERWORK"]
   

#/* Assembly-only macros */

#/* define a handy PLD(address) macro since the cache preload
# * is an optional opcode
# */
#if __ARM_HAVE_PLD
#  define  PLD(reg,offset)    pld    [reg, offset]
#else
#  define  PLD(reg,offset)    /* nothing */
#endif
    #cpp_defines += [("PLD(reg,offset)", "pld    [reg, offset]")]
    # liunote fixme -D macro!!! ???
   
"""

class sirfprima(arm1136js):    #
    platform = "sirfprima"
    virtual = False            # liunote must set not virtual for build.py MACHINE=xxx       
    c_flags = ["-Wa,-march=armv5t"]
    cpu = "sirfprima"
    cpp_defines = ["SOC_SIRF_PRIMA"]
    
class s5p6443(arm1176jzs):    #
    platform = "s5p6443"
    virtual = False            # liunote must set not virtual for build.py MACHINE=xxx       
    c_flags = ["-Wa,-march=armv5t"]
    cpu = "s5p6443"
    cpp_defines = ["SOC_S5P6443"]
    
class omap3evm(arm_cortexa8):  #(arm_cortexa8)  cross4.2.2 not support cortex-a8 ???
    platform = "omap3evm"
    virtual = False           # liunote must set not virtual for build.py MACHINE=xxx       
    c_flags = ["-Wa,-march=armv5t"]   # arm_cortexa8.c_flags +
    cpu = "omap3evm"
    cpp_defines = ["SOC_OMAP3"]    
    
