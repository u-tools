from toolchains import *
import SCons.Script
#import pdb

class Machine(object):
    virtual = True   # this is a virtual target - ie class, not instance

    default_toolchain = None
    
    as_flags = []
    c_flags = []
    cpp_flags = []
    cpp_defines = []
    cxx_flags = []
    link_flags = []

    endian = None


    cpu = ""
    smp = False # Multiple CPUs/threads?



############################################################################
# Import customer specific machines
############################################################################

import os
from types import ClassType, TypeType
def get_all_machines():

    dirs = ["tools/machines"]   # ,"projects"
    for top_dir in dirs:
        for middle_dir in os.listdir(top_dir):  # ["arm"]:     # liunote not use platform  remove ["arch","platform"]:
	    print middle_dir
            path = os.path.join(top_dir, middle_dir)
            if not os.path.exists(path):
                continue
            print path
            #for bottom_dir in os.listdir(path):

            file_name = os.path.join(path, "machine.py")  # "tools",
   	    print "liu:filename="
	    print file_name
            if os.path.exists(file_name):
	         print "os exits file name"
                 execfile(file_name, locals())

    available_machines = {}
    print os.getcwd()
    print dir()
    #pdb.set_trace()
    for each in dir():
        attr = locals()[each]
        if type(attr) in [ClassType, TypeType] and \
               issubclass(attr, Machine) and not attr.virtual:
            available_machines[each] = attr
    return available_machines


"""
    dirs = ["."] + [os.path.join("cust", x) for x in os.listdir("cust")]
    for top_dir in dirs:
        for middle_dir in ["arch", "platform"]:
            path = os.path.join(top_dir, middle_dir)
            if not os.path.exists(path):
                continue
            for bottom_dir in os.listdir(path):
                file_name = os.path.join(path, bottom_dir, "tools", "machines.py")
                if os.path.exists(file_name):
                    execfile(file_name, locals())

    available_machines = {}
    for each in dir():
        attr = locals()[each]
        if type(attr) in [ClassType, TypeType] and \
               issubclass(attr, Machine) and not attr.virtual:
            available_machines[each] = attr
    return available_machines
"""
