# Option processing functions.

import os
from types import TupleType
from xml.dom.minidom import parse
import SCons

# Make it easier to raise UserErrors
import SCons.Errors
UserError = SCons.Errors.UserError

def get_bool_arg(build, name, default):
    """Get the boolean value of an option."""
    if type(build) == dict:
        args = build
    else:
        args = build.args
    x = args.get(name, default)
    if type(x) is type(""):
        x.lower()
        if x in [1, True, "1", "true", "yes", "True"]:
            x = True
        elif x in [0, False, "0", "false", "no", "False"]:
            x = False
        else:
            raise UserError, "%s is not a valid argument for option %s. It should be True or False" % (x, name)

    return x

def get_arg(build, name, default):
    if type(build) == dict:
        args = build
    else:
        args = build.args
    return args.get(name, default)

def get_int_arg(build, name, default):
    return int(get_arg(build, name, default))

def get_option_arg(build, name, default, options, as_list = False):
    """Get the value of an option, which must be one of the supplied set"""
    if type(build) == dict:
        args = build
    else:
        args = build.args
    def_val = default
    arg = args.get(name, def_val)

    if options and type(options[0]) == TupleType:
        # This means a user has specified a list like:
        # [("foo", foo_object), ("bar", bar_object)]
        mapping = dict(options)
        options = [x[0] for x in options]
    else:
        mapping = dict([(str(x), x) for x in options])

    if arg == None:
        if str(arg) not in mapping:
            raise UserError, "need argument for option %s. Valid options are: %s" % (name, options)
        return mapping[str(arg)]

    if not isinstance(arg, str): #Assuming integer
        if arg not in options:
            raise UserError, "%s is not a valid argument for option %s. Valid options are: %s" % (x, name, options)
        return arg
    arg = arg.split(",")
    for x in arg:
        if x not in mapping:
            raise UserError, "%s is not a valid argument for option %s. Valid options are: %s" % (x, name, options)

    if as_list:
        return arg
    else:
        return mapping[arg[0]]


def get_rtos_example_args(xml_file, example_cmd_line):
    """Read the given rtos tests configuration xml file and return a dictionary 
       containing the values read"""
    rtos_args = {}
    for example in example_cmd_line:
        if not example == "all":
            rtos_args[example] = {}
            rtos_args[example]["nb_copies"] = 1
            rtos_args[example]["extra_arg"] = 1
    if os.path.exists("../../../../tools/unittest"):
        xmltree = parse("../../../../tools/unittest/" + xml_file).documentElement
    else:
        xmltree = parse("../../tools/unittest/" + xml_file).documentElement

    rtos_args["server"] = xmltree.getAttribute("server")
    example_nodes = xmltree.getElementsByTagName("example")
    for example_node in example_nodes:
        example_name = example_node.getAttribute("name")
        if example_name in example_cmd_line or example_cmd_line == ["all"]:
            if example_cmd_line == ["all"]:
                rtos_args[example_name] = {}
            rtos_args[example_name]["nb_copies"] = example_node.getAttribute("nb_copies")
            rtos_args[example_name]["extra_arg"] = example_node.getAttribute("extra_arg")
    return rtos_args

