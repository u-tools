³ò
h¬Nc           @   s§   d  Z  d Z d d d     YZ d d d     YZ y d d k Z d d k Z Wn e j
 o nA Xd e i f d	     YZ d
 d d     YZ	 d d d     YZ
 d S(   s»   SCons.Job

This module defines the Serial and Parallel classes that execute tasks to
complete a build. The Jobs class provides a higher level interface to start,
stop, and wait on jobs.

sm   /home/scons/scons/branch.0/branch.96/baseline/src/engine/SCons/Job.py 0.96.93.D001 2006/11/06 08:31:54 knightt   Jobsc           B   s    e  Z d  Z d   Z d   Z RS(   s~   An instance of this class initializes N jobs, and provides
    methods for starting, stopping, and waiting on all N jobs.
    c         C   s   d |  _ | d j o9 y t | |  |  _ | |  _ WqO t j
 o qO Xn |  i d j o t |  |  _ d |  _ n d S(   só  
        create 'num' jobs using the given taskmaster.

        If 'num' is 1 or less, then a serial job will be used,
        otherwise a parallel job with 'num' worker threads will
        be used.

        The 'num_jobs' attribute will be set to the actual number of jobs
        allocated.  If more than one job is requested but the Parallel
        class can't do it, it gets reset to 1.  Wrapping interfaces that
        care should check the value of 'num_jobs' after initialization.
        i   N(   t   Nonet   jobt   Parallelt   num_jobst	   NameErrort   Serial(   t   selft   numt
   taskmaster(    (    s   tools/SCons/Job.pyt   __init__'   s    		c         C   sP   y |  i  i   Wn8 t j
 o, d d k } | i | i | i    n Xd S(   s   run the jobiÿÿÿÿN(   R   t   startt   KeyboardInterruptt   signalt   SIGINTt   SIG_IGN(   R   R   (    (    s   tools/SCons/Job.pyt   run@   s    (   t   __name__t
   __module__t   __doc__R
   R   (    (    (    s   tools/SCons/Job.pyR    "   s   	R   c           B   s    e  Z d  Z d   Z d   Z RS(   s÷   This class is used to execute tasks in series, and is more efficient
    than Parallel, but is only appropriate for non-parallel builds. Only
    one instance of this class should be in existence at a time.

    This class is not thread safe.
    c         C   s   | |  _  d S(   s  Create a new serial job given a taskmaster. 

        The taskmaster's next_task() method should return the next task
        that needs to be executed, or None if there are no more tasks. The
        taskmaster's executed() method will be called for each task when it
        is successfully executed or failed() will be called if it failed to
        execute (e.g. execute() raised an exception).N(   R	   (   R   R	   (    (    s   tools/SCons/Job.pyR
   U   s    	c         C   s   x |  i  i   } | d j o Pn y | i   | i   Wn0 t j
 o
   n& | i   | i   n X| i   | i	   q d S(   sî   Start the job. This will begin pulling tasks from the taskmaster
        and executing them, and return when there are no more tasks. If a task
        fails to execute (i.e. execute() raises an exception), then the job will
        stop.N(
   R	   t	   next_taskR   t   preparet   executeR   t   exception_sett   failedt   executedt   postprocess(   R   t   task(    (    s   tools/SCons/Job.pyR   `   s    


(   R   R   R   R
   R   (    (    (    s   tools/SCons/Job.pyR   M   s   	iÿÿÿÿNt   Workerc           B   s    e  Z d  Z d   Z d   Z RS(   sÙ   A worker thread waits on a task to be posted to its request queue,
        dequeues the task, executes it, and posts a tuple including the task
        and a boolean indicating whether the task executed successfully. c         C   s=   t  i i |   |  i d  | |  _ | |  _ |  i   d  S(   Ni   (   t	   threadingt   ThreadR
   t	   setDaemont   requestQueuet   resultsQueueR   (   R   R    R!   (    (    s   tools/SCons/Job.pyR
      s
    		c         C   su   xn |  i  i   } y | i   Wn/ t j
 o t } n | i   d } n Xd } |  i i | | f  q d  S(   Ni    i   (   R    t   getR   R   t   FalseR   R!   t   put(   R   R   t   ok(    (    s   tools/SCons/Job.pyR      s    


(   R   R   R   R
   R   (    (    (    s   tools/SCons/Job.pyR      s   	t
   ThreadPoolc           B   s5   e  Z d  Z d   Z d   Z d d  Z d   Z RS(   sC   This class is responsible for spawning and managing worker threads.c         C   sR   t  i  d  |  _ t  i  d  |  _ x' t |  D] } t |  i |  i  q1 Wd S(   s>   Create the request and reply queues, and 'num' worker threads.i    N(   t   QueueR    R!   t   rangeR   (   R   R   t   i(    (    s   tools/SCons/Job.pyR
   ¥   s
     c         C   s   |  i  i |  d S(   s   Put task into request queue.N(   R    R$   (   R   t   obj(    (    s   tools/SCons/Job.pyR$   ®   s    i   c         C   s   |  i  i |  S(   s8   Remove and return a result tuple from the results queue.(   R!   R"   (   R   t   block(    (    s   tools/SCons/Job.pyR"   ²   s    c         C   s   |  i  i | d f  d  S(   Ni    (   R!   R$   (   R   R*   (    (    s   tools/SCons/Job.pyt   preparation_failed¶   s    (   R   R   R   R
   R$   R"   R,   (    (    (    s   tools/SCons/Job.pyR&   ¢   s
   			R   c           B   s    e  Z d  Z d   Z d   Z RS(   sº   This class is used to execute tasks in parallel, and is somewhat 
        less efficient than Serial, but is appropriate for parallel builds.

        This class is thread safe.
        c         C   s%   | |  _  t |  |  _ | |  _ d S(   s°  Create a new parallel job given a taskmaster.

            The taskmaster's next_task() method should return the next
            task that needs to be executed, or None if there are no more
            tasks. The taskmaster's executed() method will be called
            for each task when it is successfully executed or failed()
            will be called if the task failed to execute (i.e. execute()
            raised an exception).

            Note: calls to taskmaster are serialized, but calls to
            execute() on distinct tasks are not serialized, because
            that is the whole point of parallel jobs: they can execute
            multiple tasks simultaneously. N(   R	   R&   t   tpt   maxjobs(   R   R	   R   (    (    s   tools/SCons/Job.pyR
   À   s    	c         C   s2  d } x%x¤ | |  i  j  o |  i i   } | d j o Pn y | i   WnC t j
 o
   n/ | i   |  i i |  | d } q n X|  i i	 |  | d } q W| o | o Pn xc |  i i
   \ } } | d } | o | i   n | i   | i   |  i i i   o PqÈ qÈ q	 d S(   sú   Start the job. This will begin pulling tasks from the
            taskmaster and executing them, and return when there are no
            more tasks. If a task fails to execute (i.e. execute() raises
            an exception), then the job will stop.i    i   N(   R.   R	   R   R   R   R   R   R-   R,   R$   R"   R   R   R   R!   t   empty(   R   t   jobsR   R%   (    (    s   tools/SCons/Job.pyR   Ô   s:     

 


(   R   R   R   R
   R   (    (    (    s   tools/SCons/Job.pyR   ¹   s   	(    (    (    (    (   R   t   __revision__R    R   R'   R   t   ImportErrorR   R   R&   R   (    (    (    s   tools/SCons/Job.pys   <module>   s   +2