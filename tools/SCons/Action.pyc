��
h�Nc           @   sT  d  Z  d Z d d k Z d d k Z d d k Z d d k Z d d k Z d d k Z d d k l	 Z	 d d k
 Z d d k Z d d% d �  �  YZ e Z d Z d Z d Z d a d	 �  Z d
 �  Z y e i Z e i Z Wn e j
 o d �  Z n Xd �  Z d �  Z d �  Z d �  Z d d& d �  �  YZ e i i �  o) e Z  d e i i! e  f d �  �  YZ n d e f d �  �  YZ" d �  Z# d e" f d �  �  YZ$ d e f d �  �  YZ% d e% e$ f d �  �  YZ& e i i' p) e& Z  d e i i! e  f d �  �  YZ& n d e" f d �  �  YZ( d e f d  �  �  YZ) d! d' d" �  �  YZ* d# d( d$ �  �  YZ+ d S()   s�  SCons.Action

This encapsulates information about executing any sort of action that
can build one or more target Nodes (typically files) from one or more
source Nodes (also typically files) given a specific Environment.

The base class here is ActionBase.  The base class supplies just a few
OO utility methods and some generic methods for displaying information
about an Action in response to the various commands that control printing.

A second-level base class is _ActionAction.  This extends ActionBase
by providing the methods that can be used to show and perform an
action.  True Action objects will subclass _ActionAction; Action
factory class objects will subclass ActionBase.

The heavy lifting is handled by subclasses for the different types of
actions we might execute:

    CommandAction
    CommandGeneratorAction
    FunctionAction
    ListAction

The subclasses supply the following public interface methods used by
other modules:

    __call__()
        THE public interface, "calling" an Action object executes the
        command or Python function.  This also takes care of printing
        a pre-substitution command for debugging purposes.

    get_contents()
        Fetches the "contents" of an Action for signature calculation.
        This is what the Sig/*.py subsystem uses to decide if a target
        needs to be rebuilt because its action changed.

    genstring()
        Returns a string representation of the Action *without*
        command substitution, but allows a CommandGeneratorAction to
        generate the right action based on the specified target,
        source and env.  This is used by the Signature subsystem
        (through the Executor) to obtain an (imprecise) representation
        of the Action operation for informative purposes.


Subclasses also supply the following methods for internal use within
this module:

    __str__()
        Returns a string approximation of the Action; no variable
        substitution is performed.
        
    execute()
        The internal method that really, truly, actually handles the
        execution of a command or Python function.  This is used so
        that the __call__() methods can take care of displaying any
        pre-substitution representations, and *then* execute an action
        without worrying about the specific Actions involved.

    strfunction()
        Returns a substituted string representation of the Action.
        This is used by the _ActionAction.show() command to display the
        command/function that will be executed to generate the target(s).

There is a related independent ActionCaller class that looks like a
regular Action, and which serves as a wrapper for arbitrary functions
that we want to let the user specify the arguments to now, but actually
execute later (when an out-of-date check determines that it's needed to
be executed, for example).  Objects of this class are returned by an
ActionFactory class that provides a __call__() method as a convenient
way for wrapping up the functions.

sp   /home/scons/scons/branch.0/branch.96/baseline/src/engine/SCons/Action.py 0.96.93.D001 2006/11/06 08:31:54 knighti����N(   t   logInstanceCreationt   _Nullc           B   s   e  Z RS(    (   t   __name__t
   __module__(    (    (    s   tools/SCons/Action.pyR   o   s   i   i    c         C   s,   y |  i  �  SWn t j
 o |  Sn Xd  S(   N(   t   rfilet   AttributeError(   t   n(    (    s   tools/SCons/Action.pyR   z   s    c         C   s   |  S(   N(    (   t   s(    (    s   tools/SCons/Action.pyt   default_exitstatfunc�   s    c         C   s   |  S(    (    (   t   x(    (    s   tools/SCons/Action.pyt   <lambda>�   s    c         C   s�   g  } t  |  � } d } x� | | j  ou |  | } t | � } | t j o7 | t j o | i |  | | d !� n | d } q | i | � | d } q Wt i | d � S(   Ni    i   i   t    (   t   lent   ordt   HAVE_ARGUMENTt
   SET_LINENOt   appendt   stringt   join(   t   codet   resultR   t   it   ct   op(    (    s   tools/SCons/Action.pyt   remove_set_lineno_codes�   s     
c         C   s�   t  |  � } t  | � } | d  j p | d  j o# t d t |  � t | � f � n t | t � o@ t | t � o t | i | i � Sq� t | i | g � Sn9 t | t � o t | g | i � Sn t | | g � Sd  S(   Ns   Cannot append %s to %s(   t   Actiont   Nonet	   TypeErrort   typet
   isinstancet
   ListActiont   list(   t   act1t   act2t   a1t   a2(    (    s   tools/SCons/Action.pyt   _actionAppend�   s    #c         O   sq  t  |  t � o |  Sn t i i |  � o t t |  f | | � Sn t |  � oc y | d } | d =Wn t j
 o d } n X| o
 t	 } n t
 } t | |  f | | � Sn t i i |  � o� t i i |  � } | o t t | f | | � Sn t i t |  � d � } t | � d j o t t | d f | | � Sqmt | | d � | � } t | � Sn d S(   sb  This is the actual "implementation" for the
    Action factory method, below.  This handles the
    fact that passing lists to Action() itself has
    different semantics than passing lists as elements
    of lists.

    The former will create a ListAction, the latter
    will create a CommandAction by converting the inner
    list elements to strings.t	   generatori    s   
i   c         S   s   t  t |  f | | � S(    (   t   applyt   CommandAction(   R	   t   argst   kw(    (    s   tools/SCons/Action.pyR
   �   s    N(   R   t
   ActionBaset   SConst   Utilt   is_ListR&   R'   t   callablet   KeyErrort   CommandGeneratorActiont   FunctionActiont	   is_Stringt   get_environment_vart
   LazyActionR   t   splitt   strR   t   mapR   R   (   t   actR(   R)   t   gent   action_typet   vart   commandst   listCmdActions(    (    s   tools/SCons/Action.pyt   _do_create_action�   s2    

	c         O   s�   t  i i |  � oT t | | d � |  � } t d | � } t | � d j o | d Sq~ t | � Sn t t	 |  f | | � Sd S(   s   A factory for action objects.c         S   s   t  t |  f | | � S(    (   R&   R>   (   t   aR(   R)   (    (    s   tools/SCons/Action.pyR
   �   s    i   i    N(
   R+   R,   R-   R7   t   filterR   R   R   R&   R>   (   R8   R(   R)   t   acts(    (    s   tools/SCons/Action.pyR   �   s    	R*   c           B   sa   e  Z d  Z e i i o e i i Z n d �  Z d �  Z	 d �  Z
 d �  Z d �  Z d �  Z RS(   s�   Base class for all types of action objects that can be held by
    other objects (Builders, Executors, etc.)  This provides the
    common methods for manipulating and combining those actions.c         C   s   t  |  i | � S(   N(   t   cmpt   __dict__(   t   selft   other(    (    s   tools/SCons/Action.pyt   __cmp__�   s    c         C   s
   t  |  � S(   N(   R6   (   RD   t   targett   sourcet   env(    (    s   tools/SCons/Action.pyt	   genstring�   s    c         C   s   t  |  | � S(   N(   R$   (   RD   RE   (    (    s   tools/SCons/Action.pyt   __add__�   s    c         C   s   t  | |  � S(   N(   R$   (   RD   RE   (    (    s   tools/SCons/Action.pyt   __radd__�   s    c         C   s.   | |  _  t i t |  � d � } d  |  _  | S(   Ns   
(   t
   presub_envR   R5   R6   R   (   RD   RI   t   lines(    (    s   tools/SCons/Action.pyt   presub_lines�   s    		c         C   s   t  i i |  | | | | | � S(   s$   Return the Executor for this Action.(   R+   t   Executor(   RD   RI   t	   overridest   tlistt   slistt   executor_kw(    (    s   tools/SCons/Action.pyt   get_executor  s    (   R   R   t   __doc__R+   t   Memoizet   use_memoizert   Memoized_Metaclasst   __metaclass__RF   RJ   RK   RL   RO   RU   (    (    (    s   tools/SCons/Action.pyR*   �   s   					c           B   s   e  Z d  Z d �  Z RS(   s"   Cache-backed version of ActionBasec         O   s1   t  t i |  f | | � t i i i |  � d  S(   N(   R&   t   _Baset   __init__R+   RW   t   Memoizer(   RD   R(   R)   (    (    s   tools/SCons/Action.pyR\     s    (   R   R   RV   R\   (    (    (    s   tools/SCons/Action.pyR*     s   t   _ActionActionc           B   sD   e  Z d  Z e e d d d � Z d �  Z e e e e e d � Z RS(   s2   Base class for actions that create output objects.c         K   sa   | t  j	 o | |  _ n | t  j o
 t } n | |  _ | |  _ | p
 t } n | |  _ d  S(   N(   t   _nullt   strfunctiont   print_actions_presubt   presubt   chdirR   t   exitstatfunc(   RD   R`   Rb   Rc   Rd   R)   (    (    s   tools/SCons/Action.pyR\     s    
		
c         C   s   t  i i | d � d  S(   Ns   
(   t   syst   stdoutt   write(   RD   R   RG   RH   RI   (    (    s   tools/SCons/Action.pyt   print_cmd_line#  s    c	         C   s�  t  i i | � p | g } n t  i i | � p | g } n | t j o |  i } n | t j o |  i } n | t j o
 t } n | t j o
 t } n | t j o |  i } n d  }	 | oc t
 i �  }	 y t | i � } Wq,t j
 o1 t  i i | � p t | d i � } q(q,Xn | oZ t i t t | � d � }
 t i |  i | � d � } d |
 | f } t i i | � n d  } | o� |  i o� |  i | | | � } | o� | o d t | � | } n y | i } Wn t j
 o |  i } n" X| d � } | p |  i } n | | | | | � qFn d } | o] | o t
 i | � n z% |  i | | | � } | | � } Wd  |	 o t
 i |	 � n Xn | o( |	 o! | d t |	 � | | | � n | S(   Ni    s    and s   
  s   Building %s with action:
  %s
s   os.chdir(%s)
t   PRINT_CMD_LINE_FUNCs   os.chdir(%s)(   R+   R,   R-   R_   Rd   Rb   t   print_actionst   execute_actionsRc   R   t   ost   getcwdR6   t   abspathR   R2   t   dirR   R   R7   RO   Re   Rf   Rg   R`   t   reprt   getRh   t   execute(   RD   RG   RH   RI   Rd   Rb   t   showRr   Rc   t   save_cwdt   tt   lt   outR   Rq   t
   print_funct   stat(    (    s   tools/SCons/Action.pyt   __call__&  sj       
 
  !N(   R   R   RV   R_   R   R\   Rh   Rz   (    (    (    s   tools/SCons/Action.pyR^     s   	c         C   sf   g  } xP t  t |  � D]? } d | j p d | j o d | d } n | i | � q Wt i | � S(   s\   Takes a list of command line arguments and returns a pretty
    representation for printing.t    s   	t   "(   R7   R6   R   R   R   (   t   cmd_listt   clt   arg(    (    s   tools/SCons/Action.pyt   _string_from_cmd_list`  s     R'   c           B   sG   e  Z d  Z d d � Z d �  Z d �  Z d �  Z d �  Z d �  Z	 RS(   s$   Class for command-execution actions.c         O   s�   t  |  d � | d  j	 oK t | � o | f | } qe t i i | � p t i i d � � qe n t t	 i
 |  f | | � t i i | � o' t t i i | � o t d � q� n | |  _ | |  _ d  S(   Ns   Action.CommandActions�   Invalid command display variable type. You must either pass a string or a callback which accepts (target, source, env) as parameters.s3   CommandAction should be given only a single command(   R    R   R.   R+   R,   R2   t   Errorst	   UserErrorR&   R^   R\   R-   R@   R   R}   t   cmdstr(   RD   t   cmdR�   R(   R)   (    (    s   tools/SCons/Action.pyR\   l  s    
		c         C   sC   t  i i |  i � o  t i t t |  i � d � Sn t |  i � S(   NR{   (   R+   R,   R-   R}   R   R   R7   R6   (   RD   (    (    s   tools/SCons/Action.pyt   __str__�  s     c         C   s�   | i  |  i d | | � } d  } d  } x~ y | d d d } Wn t j
 o d  } n X| d j o
 d } n | d j o
 d } n P| d d d | d d <q* y) | d d p | d d | d <n Wn t j
 o n X| | | f S(   Ni    t   @i   t   -(   t
   subst_listR}   R   t
   IndexError(   RD   RG   RH   RI   R   t   silentt   ignoreR   (    (    s   tools/SCons/Action.pyt   process�  s(       
 
c         C   s�   |  i  d  j	 oH | i d t � o4 | i |  i  t i i | | � } | o | SqX n |  i | | | � \ } } } | o d Sn t	 | d � S(   Nt   VERBOSE_STRR   i    (
   R�   R   Rq   t   Falset   substR+   t   Substt	   SUBST_RAWR�   R�   (   RD   RG   RH   RI   R   R}   R�   R�   (    (    s   tools/SCons/Action.pyR`   �  s    $!c         C   s  d d k  l } d d k l } l } l } y | d } Wn% t j
 o |	 i i d � � n Xy | d }
 Wn% t j
 o |	 i i d � � n X| i	 d d	 �  � } y | d
 } WnC t j
 o7 t
 p# d d k }	 |	 i i �  d
 a
 n t
 } n Xxz | i �  D]l \ } } | | � pS | | � o2 | | � } t i t t | � t i � | | <qzt | � | | <qqW|  i | t t | � | � \ } } } xY t t | � D]H } | | | � } |
 | | | d | | � } | o | o | Sq�q�Wd S(   sV  Execute a command action.

        This will handle lists of commands as well as individual commands,
        because construction variable substitution may turn a single
        "command" into a list.  This means that this class can actually
        handle lists of commands, even though that's not how we use it
        externally.
        i����(   t   escape_list(   R2   R-   t   flattent   SHELLs$   Missing SHELL construction variable.t   SPAWNs$   Missing SPAWN construction variable.t   ESCAPEc         S   s   |  S(    (    (   R	   (    (    s   tools/SCons/Action.pyR
   �  s    t   ENVNi    (   t   SCons.SubstR�   t
   SCons.UtilR2   R-   R�   R/   R�   R�   Rq   t   default_ENVt   SCons.Environmentt   Environmentt   itemsR   R   R7   R6   Rl   t   pathsepR�   R   R@   R   (   RD   RG   RH   RI   R�   R2   R-   R�   t   shellR+   t   spawnt   escapeR�   t   keyt   valueR}   R�   R�   t   cmd_lineR   (    (    s   tools/SCons/Action.pyRr   �  sB    	 &' c         C   sj   d d k  l } |  i } t i i | � o t i t t	 | � � } n t	 | � } | i
 | | | | � S(   s�   Return the signature contents of this action's command line.

        This strips $(-$) and everything in between the string,
        since those parts don't affect signatures.
        i����(   t	   SUBST_SIG(   R�   R�   R}   R+   R,   R-   R   R   R7   R6   t   subst_target_source(   RD   RG   RH   RI   R�   R�   (    (    s   tools/SCons/Action.pyt   get_contents�  s    	N(
   R   R   RV   R   R\   R�   R�   R`   Rr   R�   (    (    (    s   tools/SCons/Action.pyR'   j  s   			
	<R0   c           B   sS   e  Z d  Z d �  Z d �  Z d �  Z d �  Z e e e e e d � Z d �  Z	 RS(   s$   Class for command-generator actions.c         O   s,   t  |  d � | |  _ | |  _ | |  _ d  S(   Ns   Action.CommandGeneratorAction(   R    R%   t   gen_argst   gen_kw(   RD   R%   R(   R)   (    (    s   tools/SCons/Action.pyR\   �  s    		c      	   C   s�   t  i i | � p | g } n |  i d | d | d | d | � } t t | f |  i |  i � } | p  t  i i	 d t
 | � � � n | S(   NRG   RH   RI   t   for_signaturesN   Object returned from command generator: %s cannot be used to create an Action.(   R+   R,   R-   R%   R&   R   R�   R�   R�   R�   Rp   (   RD   RG   RH   RI   R�   t   rett   gen_cmd(    (    s   tools/SCons/Action.pyt	   _generate�  s    $ c         C   sR   y |  i  p h  } Wn t j
 o h  } n X|  i g  g  | d � } t | � S(   Ni   (   RM   R   R�   R6   (   RD   RI   R8   (    (    s   tools/SCons/Action.pyR�     s    c         C   s%   |  i  | | | d � i | | | � S(   Ni   (   R�   RJ   (   RD   RG   RH   RI   (    (    s   tools/SCons/Action.pyRJ     s    c	   
   	   C   s7   |  i  | | | d � }	 |	 | | | | | | | | � S(   Ni    (   R�   (
   RD   RG   RH   RI   Rd   Rb   Rs   Rr   Rc   R8   (    (    s   tools/SCons/Action.pyRz     s    c         C   s%   |  i  | | | d � i | | | � S(   s�   Return the signature contents of this action's command line.

        This strips $(-$) and everything in between the string,
        since those parts don't affect signatures.
        i   (   R�   R�   (   RD   RG   RH   RI   (    (    s   tools/SCons/Action.pyR�     s    (
   R   R   RV   R\   R�   R�   RJ   R_   Rz   R�   (    (    (    s   tools/SCons/Action.pyR0   �  s   				R4   c           B   s[   e  Z e i i o e i i Z n d  �  Z d �  Z d �  Z	 d �  Z
 d �  Z d �  Z RS(   c         O   sY   t  |  d � t t i |  d | f | | � t i i | � |  _ | |  _ | |  _	 d  S(   Ns   Action.LazyActiont   $(
   R    R&   R'   R\   R+   R,   t	   to_StringR;   R�   R�   (   RD   R;   R(   R)   (    (    s   tools/SCons/Action.pyR\   9  s
    !	c         C   s>   | i  |  i � } t i i | � o d | j o t Sn t S(   Ns   
(   Rq   R;   R+   R,   R2   R'   R0   (   RD   RI   R   (    (    s   tools/SCons/Action.pyt   get_parent_class@  s     c         C   sh   | i  |  i d � } t t | f |  i |  i � } | p) t i i d |  i t	 | � f � � n | S(   t   __cacheable__R   s0   $%s value %s cannot be used to create an Action.(
   Rq   R;   R&   R   R�   R�   R+   R�   R�   Rp   (   RD   RI   R   R�   (    (    s   tools/SCons/Action.pyt   _generate_cacheF  s
    )c         C   s   |  i  | � S(   N(   R�   (   RD   RG   RH   RI   R�   (    (    s   tools/SCons/Action.pyR�   N  s    c         O   s8   |  | | | f | } |  i  | � } t | i | | � S(   N(   R�   R&   Rz   (   RD   RG   RH   RI   R(   R)   R   (    (    s   tools/SCons/Action.pyRz   Q  s    c         C   s%   |  i  | � } | i |  | | | � S(   N(   R�   R�   (   RD   RG   RH   RI   R   (    (    s   tools/SCons/Action.pyR�   V  s    (   R   R   R+   RW   RX   RY   RZ   R\   R�   R�   R�   Rz   R�   (    (    (    s   tools/SCons/Action.pyR4   4  s   					c           B   s   e  Z d  �  Z RS(   c         O   s1   t  i i i |  � t t i |  f | | � d  S(   N(   R+   RW   R]   R\   R&   R[   (   RD   R(   R)   (    (    s   tools/SCons/Action.pyR\   ]  s    (   R   R   R\   (    (    (    s   tools/SCons/Action.pyR4   \  s   R1   c           B   sG   e  Z d  Z e d � Z d �  Z d �  Z d �  Z d �  Z d �  Z	 RS(   s"   Class for Python function actions.c         O   s�   t  |  d � | t j	 oX t | � o | f | } qr | d  j p t i i | � p t i i d � � qr n | |  _	 t
 t i |  f | | � | i d g  � |  _ | |  _ d  S(   Ns   Action.FunctionActions�   Invalid function display variable type. You must either pass a string or a callback which accepts (target, source, env) as parameters.t   varlist(   R    R_   R.   R   R+   R,   R2   R�   R�   t   execfunctionR&   R^   R\   Rq   R�   R�   (   RD   R�   R�   R(   R)   (    (    s   tools/SCons/Action.pyR\   f  s     		c         C   sS   y |  i  i SWn> t j
 o2 y |  i  i i SWqO t j
 o d SqO Xn Xd  S(   Nt   unknown_python_function(   R�   R   R   t	   __class__(   RD   (    (    s   tools/SCons/Action.pyt   function_namew  s    c   
      C   s
  |  i  d  j o d  Sn |  i  t j	 oH | i d t � o4 | i |  i  t i i | | � } | o | Sqp n d �  } y |  i	 i
 } Wn t j
 o n8 X| d  j o d  Sn t | � o | | | | � Sn |  i �  } | | � } | | � }	 d | | |	 f S(   NR�   c         S   s*   d �  } d t  i t | |  � d � d S(   Nc         S   s   d t  |  � d S(   NR|   (   R6   (   R   (    (    s   tools/SCons/Action.pyt   quote�  s    t   [s   , t   ](   R   R   R7   (   R?   R�   (    (    s   tools/SCons/Action.pyt   array�  s    	s
   %s(%s, %s)(   R�   R   R_   Rq   R�   R�   R+   R�   R�   R�   R`   R   R.   R�   (
   RD   RG   RH   RI   R   R�   t   strfunct   namet   tstrt   sstr(    (    s   tools/SCons/Action.pyR`   �  s&    $!	c         C   s2   |  i  �  } | d j o t |  i � Sn d | S(   Nt   ActionCallers   %s(target, source, env)(   R�   R6   R�   (   RD   R�   (    (    s   tools/SCons/Action.pyR�   �  s    c      
   C   s�   t  t | � } y" |  i d | d | d | � } Wnb t j
 oV } y | i } Wn t j
 o d  } n Xt i i	 d | d | i
 d | � � n X| S(   NRG   RH   RI   t   nodet   errstrt   filename(   R7   R   R�   t   EnvironmentErrorR�   R   R   R+   R�   t
   BuildErrort   strerror(   RD   RG   RH   RI   t   rsourcesR   t   eR�   (    (    s   tools/SCons/Action.pyRr   �  s    "  	c      	   C   s$  |  i  } y | i i } Wn� t j
 o� y | i i i } Wn� t j
 o� y | i i i i } WnZ t j
 oN y |  i  i } Wn" t j
 o t |  i  � } q� X| | | | � } q� Xt | � } q� Xt | � } n Xt | � } t | � } | | i	 t
 i t d �  |  i � � � S(   s�  Return the signature contents of this callable action.

        By providing direct access to the code object of the
        function, Python makes this extremely easy.  Hooray!

        Unfortunately, older versions of Python include line
        number indications in the compiled byte code.  Boo!
        So we remove the line number byte codes to prevent
        recompilations from moving a Python function.
        c         S   s   d  |  d S(   s   ${t   }(    (   t   v(    (    s   tools/SCons/Action.pyR
   �  s    (   R�   t	   func_codet   co_codeR   t   im_funcRz   R�   R6   R   R�   R   R   R7   R�   (   RD   RG   RH   RI   R�   R   t   gct   contents(    (    s   tools/SCons/Action.pyR�   �  s*    	(
   R   R   RV   R_   R\   R�   R`   R�   Rr   R�   (    (    (    s   tools/SCons/Action.pyR1   c  s   					R   c           B   sS   e  Z d  Z d �  Z d �  Z d �  Z d �  Z d �  Z e e e e e d � Z	 RS(   s!   Class for lists of other actions.c         C   s,   t  |  d � d �  } t | | � |  _ d  S(   Ns   Action.ListActionc         S   s"   t  |  t � o |  Sn t |  � S(   N(   R   R*   R   (   R	   (    (    s   tools/SCons/Action.pyt   list_of_actions�  s    (   R    R7   R   (   RD   R   R�   (    (    s   tools/SCons/Action.pyR\   �  s    	c         C   s(   t  i t | | | d � |  i � d � S(   Nc         S   s   |  i  | | | � S(    (   RJ   (   R?   Ru   R   R�   (    (    s   tools/SCons/Action.pyR
   �  s    s   
(   R   R   R7   R   (   RD   RG   RH   RI   (    (    s   tools/SCons/Action.pyRJ   �  s    	c         C   s   t  i t t |  i � d � S(   Ns   
(   R   R   R7   R6   R   (   RD   (    (    s   tools/SCons/Action.pyR�   �  s    c         C   s"   t  i i t | d � |  i � � S(   Nc         S   s   |  i  | � S(    (   RO   (   R?   RI   (    (    s   tools/SCons/Action.pyR
   �  s    (   R+   R,   R�   R7   R   (   RD   RI   (    (    s   tools/SCons/Action.pyRO   �  s    c         C   s(   t  i t | | | d � |  i � d � S(   s|   Return the signature contents of this action list.

        Simple concatenation of the signatures of the elements.
        c         S   s   |  i  | | | � S(    (   R�   (   R	   Ru   R   R�   (    (    s   tools/SCons/Action.pyR
   �  s    R   (   R   R   R7   R   (   RD   RG   RH   RI   (    (    s   tools/SCons/Action.pyR�   �  s    	c	      
   C   sH   xA |  i  D]6 }	 |	 | | | | | | | | � }
 |
 o |
 Sq
 q
 Wd S(   Ni    (   R   (   RD   RG   RH   RI   Rd   Rb   Rs   Rr   Rc   R8   Ry   (    (    s   tools/SCons/Action.pyRz   �  s    
 (
   R   R   RV   R\   RJ   R�   RO   R�   R_   Rz   (    (    (    s   tools/SCons/Action.pyR   �  s   					
R�   c           B   sV   e  Z d  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z d �  Z	 d �  Z
 RS(	   sc  A class for delaying calling an Action function with specific
    (positional and keyword) arguments until the Action is actually
    executed.

    This class looks to the rest of the world like a normal Action object,
    but what it's really doing is hanging on to the arguments until we
    have a target, source and env to use for the expansion.
    c         C   s   | |  _  | |  _ | |  _ d  S(   N(   t   parentR(   R)   (   RD   R�   R(   R)   (    (    s   tools/SCons/Action.pyR\     s    		c         C   s�   |  i  i } y t | i i � } WnQ t j
 oE y t | i i i i � } Wqv t j
 o t | � } qv Xn Xt | � } | S(   N(	   R�   t   actfuncR6   R�   R�   R   Rz   R�   R   (   RD   RG   RH   RI   R�   R�   (    (    s   tools/SCons/Action.pyR�     s    c         C   s/   | d j o | Sn | i  | d | | � Sd  S(   Ns   $__env__i    (   R�   (   RD   R   RG   RH   RI   (    (    s   tools/SCons/Action.pyR�   !  s    c         C   s   t  |  | | | d � |  i � S(   Nc         S   s   | i  |  | | | � S(    (   R�   (   R	   RD   Ru   R   R�   (    (    s   tools/SCons/Action.pyR
   *  s    (   R7   R(   (   RD   RG   RH   RI   (    (    s   tools/SCons/Action.pyt
   subst_args)  s    c         C   sG   h  } x: |  i  i �  D]) } |  i |  i  | | | | � | | <q W| S(   N(   R)   t   keysR�   (   RD   RG   RH   RI   R)   R�   (    (    s   tools/SCons/Action.pyt   subst_kw-  s
     'c         C   s@   |  i  | | | � } |  i | | | � } t |  i i | | � S(   N(   R�   R�   R&   R�   R�   (   RD   RG   RH   RI   R(   R)   (    (    s   tools/SCons/Action.pyRz   2  s    c         C   s@   |  i  | | | � } |  i | | | � } t |  i i | | � S(   N(   R�   R�   R&   R�   R�   (   RD   RG   RH   RI   R(   R)   (    (    s   tools/SCons/Action.pyR`   6  s    c         C   s   t  |  i i |  i |  i � S(   N(   R&   R�   R�   R(   R)   (   RD   (    (    s   tools/SCons/Action.pyR�   :  s    (   R   R   RV   R\   R�   R�   R�   R�   Rz   R`   R�   (    (    (    s   tools/SCons/Action.pyR�     s   							t   ActionFactoryc           B   s    e  Z d  Z d �  Z d �  Z RS(   sf  A factory class that will wrap up an arbitrary function
    as an SCons-executable Action object.

    The real heavy lifting here is done by the ActionCaller class.
    We just collect the (positional and keyword) arguments that we're
    called with and give them to the ActionCaller object we create,
    so it can hang onto them until it needs them.
    c         C   s   | |  _  | |  _ d  S(   N(   R�   R�   (   RD   R�   R�   (    (    s   tools/SCons/Action.pyR\   F  s    	c         O   s+   t  |  | | � } t | d | i �} | S(   NR`   (   R�   R   R`   (   RD   R(   R)   t   act   action(    (    s   tools/SCons/Action.pyRz   I  s    (   R   R   RV   R\   Rz   (    (    (    s   tools/SCons/Action.pyR�   =  s   	(    (    (    (    (,   RV   t   __revision__t   disRl   t   os.patht   reR   Re   t   SCons.DebugR    t   SCons.ErrorsR+   R�   R   R_   Rj   Rk   Ra   R   R�   R   R   R   R   R   R   R$   R>   R   R*   RW   t   use_old_memoizationR[   R]   R^   R�   R'   R0   R4   t   has_metaclassR1   R   R�   R�   (    (    (    s   tools/SCons/Action.pys   <module>I   sV   						.	%#J	
�@&#w+8