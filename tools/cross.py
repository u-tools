import os
import copy
import sets
import time
from stat import ST_SIZE
from cStringIO import StringIO
from math import ceil, log
#import commandscust
#from xml.dom.minidom import getDOMImplementation, DocumentType

import machines
import toolchains
from opts import get_int_arg, get_bool_arg, get_arg, get_option_arg
from util import markup, xmlbool

from SCons.Util import flatten
import tarfile
import re

# Make it easier to raise UserErrors
import SCons.Errors
UserError = SCons.Errors.UserError


def get_toolchain(name, machine):
    """
    Return a toolchain object and its name.

    If name is not None, then it must be the name of a valid toolchain.
    If name is None, the default toolchain for the machine is used (assuming it exists).
    """
    if name is not None:
        available_toolchains = toolchains.get_all_toolchains()
        if name not in available_toolchains:
            raise UserError, ("'%s' is not a valid toolchain. Must be one of: %s" % (name, available_toolchains.keys()))
        toolchain = available_toolchains[name]
    else:
        try:
            toolchain = machine.default_toolchain
            name = "default"
        except AttributeError:
            raise UserError, "No TOOLCHAIN specified. You did not specify " + \
                  "a toolchain, and the machine you chose \n\tdoes not " + \
                  "have a default one. Must be one of: %s" % available_toolchains.keys()
    return toolchain, name

class CrossBuild:
    """Top level class in the hierarchy of instantiated classes. This
    represents the top of the build."""
    def __init__(self, project = None, **kargs):
        # Any default arguments from the SConstruct are overridden by the user
        kargs.update(conf.dict)

        def handle_arg(name, default):
            ret = kargs.get(name, default)
            if name in kargs: del kargs[name]
            return ret

        self.build_dir = handle_arg("BUILD_DIR", "#build")

	# liunote resever top build dir
	#self.top_build_dir = self.build_dir

        # nouse liunote get PROJECT_DIR 
	#self.project_dir = handle_arg("PROJECT_DIR", "#build")

	#self.top
        if not self.build_dir.startswith('#'):
            self.build_dir = '#' + self.build_dir

        # Here we setup the sconsign file. This is the database in which
        # SCons holds file signatures. (See man scons for more information)
        if not os.path.exists(Dir(self.build_dir).abspath):
	    # liutest top build mkdir
            os.mkdir(Dir(self.build_dir).abspath)
        SConsignFile(Dir(self.build_dir).abspath + os.sep + "sconsign")

        available_machines = machines.get_all_machines()
        sorted_avail_machines = available_machines.keys()
        sorted_avail_machines.sort()

        machine = handle_arg("MACHINE", None)
        if machine is None:
            raise UserError, "You must specify the MACHINE you " + \
                  "wish to build for.  Must be one of: %s" % sorted_avail_machines
        if machine not in available_machines:
            raise UserError, ("'%s' is not a valid machine. Must be one of: %s" % \
                              (machine, sorted_avail_machines))
        self.machine = available_machines[machine]
        self.machine_name = machine
        self.project = project        # liunote use project for rootfs build,other build etc !!!
        
        toolchain = handle_arg("TOOLCHAIN", None)
        if toolchain is not None:
            self.toolchain_name = toolchain
        else:
            self.toolchain_name = "gnu_arm_eabi_toolchain"
        self.toolchain, toolchain = get_toolchain(toolchain, self.machine)

        self.image_type = handle_arg("IMAGE_TYPE", "generic")         #liunote image_type used ???

        # Any remaining options are saved, as they may be passed down to
        # CrossEnvironment's created.
        self.args = kargs
	#print "CrossBuild"
	#print self.args

        # Initialise the expected test output list.
        self.expect_test_data = []

    #a(*args)=>a(1,2,3) tuple:(1, 2, 3) a(**args)=>a(x=1,y=2) dict:{'y': 2, 'x': 1}    

    def CrossEnvironment(self, name, *args, **kargs):
        """Create a new environment in this build.
        Name is the name of the environment to create.
        *args are passed on to the CrossEnvironment class.
        **kargs are any options configuration options."""
        # Create a copy of our extra args, and then update first
        # with any overriders from the kargs, and then from the user's
        # conf
        new_args = copy.copy(self.args)      # copy args to new obj 
        new_args.update(kargs)               # merge kargs !!!

        # If the user has specified any <name>.<foo> options pass
        # them through.
        if hasattr(conf, name):
            new_args.update(getattr(conf, name).dict)  # merge conf.xxx , <name>.<foo> name must = enviroment name!!!
        

        # Finally update these required fields, with sane defaults,
        # only if they haven't already been overridden.
        new_args["SYSTEM"] = new_args.get("SYSTEM", name)
        new_args["MACHINE"] = new_args.get("MACHINE", self.machine)
        new_args["TOOLCHAIN"] = new_args.get("TOOLCHAIN", self.toolchain)
        new_args["BUILD_DIR"] = new_args.get("BUILD_DIR", self.build_dir +
                                             os.sep + name)
        # liunote top build dir
	new_args["TOP_BUILD_DIR"] = new_args.get("TOP_BUILD_DIR", self.build_dir)
	#print new_args["TOP_BUILD_DIR"]

        # nouse liunote add project dir for build
        #new_args["PROJECT_DIR"] = new_args.get("PROJECT_DIR", self.project_dir)

        return CrossEnvironment(name, *args, **new_args)

    def get_run_method(self):
        if len(self.machine.run_methods.keys()) > 1:
            key = conf.dict.get('RUN', self.machine.default_method)
        else:
            key = self.machine.run_methods.keys()[0]
        return self.machine.run_methods[key]


def env_inner_class(cls):
    """
    Grant a class access to the attributes of a CrossEnvironment
    instance.  This is similar to Java inner classes.

    The weaver objects need access to the machine properties, which
    are stored in CrossEnvironment.machine.  Rather then requiring the
    env to be passed to every constructor,  this function converts
    them into inner classes, so that env.Foo() will create an instance
    of Foo that contains a __machine__ class attribute.
    """
    # Sanity Check
    if hasattr(cls, '__machine__'):
        raise TypeError('Existing attribute "__machine__" '
                        'in inner class')

    class EnvDescriptor(object):
        def __get__(self, env, envclass):
            if env is None:
                raise AttributeError('An enclosing instance for %s '
                                     'is required' % (cls.__name__))
            classdict = cls.__dict__.copy()
            # Add the env's machine attr to the class env.
            classdict['__machine__'] = property(lambda s: env.machine)

            return type(cls.__name__, cls.__bases__, classdict)

    return EnvDescriptor()

class CrossEnvironment:
    """The CrossEnvironment creates the environment in which to build
    applications and libraries. he main idea is that everything in an
    environment shares the same toolchain and set of libraries.
    """
    c_ext = ["c"]
    cxx_ext = ["cc", "cpp", "c++"]
    asm_ext = ["s"]
    cpp_asm_ext = ["spp"]
    src_exts = c_ext + cxx_ext + asm_ext + cpp_asm_ext

    def __init__(self, name, SYSTEM, MACHINE, TOOLCHAIN,
                 BUILD_DIR,TOP_BUILD_DIR,PROJECT_DIR, **kargs):
        """Create a new environment called name for a given SYSTEM, 
        MACHINE and TOOLCHAIN. It will be built into the specified BUILD_DIR.
        Any extra kargs are saved as extra options to pass on to Packages."""

        self.scons_env = Environment() # The underlying SCons environment.
        self.name = name
        self.system = SYSTEM
        self.machine = MACHINE
        self.toolchain = copy.deepcopy(TOOLCHAIN)
        self.builddir = BUILD_DIR
        self.args = kargs
        self.test_libs = []
        self.linux_dir = 'linux'
	self.project_dir = PROJECT_DIR

        # liunote add top build dir
	self.top_build_dir = TOP_BUILD_DIR

        if kargs.has_key('LINUX_DIR'):
            self.linux_dir = kargs['LINUX_DIR']
            del kargs['LINUX_DIR']

        # place project for build (rootfs etc ...)
        if kargs.has_key('PROJECT_DIR'):
            self.project_dir = kargs['PROJECT_DIR']
            del kargs['PROJECT_DIR']

        #liunote add top build dir
        if kargs.has_key('TOP_BUILD_DIR'):
            self.top_build_dir = kargs['TOP_BUILD_DIR']
            del kargs['TOP_BUILD_DIR']

        def _installString(target, source, env):
            if env.get("VERBOSE_STR", False):
                return "cp %s %s" % (source, target)
            else:
                return "[INST] %s" % target
        self.scons_env["INSTALLSTR"] = _installString

        # Setup toolchain.

        if type(self.toolchain) == type(""):
            self.toolchain, _ = get_toolchain(self.toolchain, self.machine)
        for key in self.toolchain.dict.keys():
            self.scons_env[key] = copy.copy(self.toolchain.dict[key])

        def _libgcc(env):
            """Used by some toolchains to find libgcc."""
            return os.popen(env.subst("$CC $CCFLAGS --print-libgcc-file-name")). \
                   readline()[:-1]
        self.scons_env["_libgcc"] = _libgcc        # liunote now not use samsung 4.2.2 libgcc ???

        def _libgcc_eh(env):
            """Used by some toolchains to find libgcc_eh."""
            return os.popen(env.subst("$CC $CCFLAGS --print-libgcc-file-name")). \
                   readline()[:-1].replace("libgcc", "libgcc_eh")
        self.scons_env["_libgcc_eh"] = _libgcc_eh

        def _machine(arg):
            """Return a machine attribute. Used by the toolchains to get
            compiler/machine specific flags."""
            return getattr(self.machine, arg)
        self.scons_env["_machine"] = _machine

        self.headers = []
        self.libs = []
        
        #liunote for share lib
        self.shlibs = []
        

        # Pass through the user's environment variables to the build
        self.scons_env['ENV'] = copy.copy(os.environ)

        # Setup include path
	      # print "Setup include path:"
	      # print self.builddir  + os.sep + "include"
        self.scons_env.Append(CPPPATH = self.builddir  + os.sep + "include")
        self.scons_env["LIBPATH"] = [self.builddir  + os.sep + "lib"]

        # Setup machine defines. Don't really like this. Maybe config.h
        self.scons_env.Append(CPPDEFINES =
                              ["ARCH_%s" % self.machine.arch.upper()])
        if hasattr(self.machine, "arch_version"):
            self.scons_env.Append(CPPDEFINES =
                    [("ARCH_VER", self.machine.arch_version)])
        # FIXME: deal with variant machine names (philipo)
        name = self.machine.__name__.upper()
        if name.endswith('_HW'):
            name = name[:-3]

        # liunote not use this CPPDEFINES for gcc machine def ???
	self.scons_env.Append(CPPDEFINES = ["MACHINE_%s" % name])
        self.scons_env.Append(CPPDEFINES =
                              ["ENDIAN_%s" % self.machine.endian.upper()])
        self.scons_env.Append(CPPDEFINES =
                              ["WORDSIZE_%s" % self.machine.wordsize])
        self.scons_env.Append(CPPDEFINES = self.machine.cpp_defines)
        self.scons_env.Append(CPPDEFINES = self.machine.cpp_defines)
        if self.machine.smp:
            self.scons_env.Append(CPPDEFINES = ["MACHINE_SMP"])
        
	# liunote used ??? 
        #self.scons_env.Append(CPPDEFINES =
        #                      ["CROSS_%s" % self.name.upper()])


        # These should use the Scons SubstitutionEnvironment functions !!
        def _linkaddress(prefix, linkfile):
            addr = eval(open(linkfile.abspath).read().strip())
            return "%s%x" % (prefix, addr)
        self.scons_env["_linkaddress"] = _linkaddress

        def _findlib(inp):
            ret = [self.scons_env.FindFile(
                self.scons_env.subst("${LIBPREFIX}%s${LIBSUFFIX}" % x),
                self.scons_env.subst("$LIBPATH"))
                   for x in inp]
            return ret
        self.scons_env["_findlib"] = _findlib

        def _as_string(inp):
            """Convert a list of things, to a list of strings. Used by
            for example the LINKSCRIPTS markup to convert a list of
            files into a list of strings."""
            return map(str, inp)
        self.scons_env["_as_string"] = _as_string

        def _platform(type, flags):
            """Return the compile flag for the requested compiler"""
            if type == "gnu":
                if flags == "c_flags":
                    return self.machine.c_flags
                # do we want separate cxx_flags?
                if flags == "cxx_flags":
                    return self.machine.c_flags
                if flags == "as_flags":
                    return self.machine.as_flags
                if flags == "link_flags":
                    return self.machine.link_flags
            if type == "rvct":
                if flags == "c_flags":
                    return self.machine.rvct_c_flags
                # do we want separate cxx_flags?
                if flags == "cxx_flags":
                    return self.machine.rvct_c_flags
                if flags == "as_flags":
                    return self.machine.rvct_c_flags
            if type == "ads":
                if flags == "c_flags":
                    return self.machine.ads_c_flags
                # do we want separate cxx_flags?
                if flags == "cxx_flags":
                    return self.machine.ads_c_flags
                if flags == "as_flags":
                    return self.machine.ads_c_flags

            return ""
        self.scons_env["_platform"] = _platform

        # Record revision control information.  Assume that it is from
        # mercurial if the .hg directory is present.
        if os.access('../../.hg', os.F_OK):
            hg_config = commands.getoutput("hg showconfig")

            for line in hg_config.split("\n"):
                params = line.split("=")

                if params[0] == "paths.default":
                    self.scons_env['REVISION'] = params[1]
                    self.scons_env['TAG'] = commands.getoutput("hg identify -i")
                    break
        else:
            self.scons_env['REVISION'] = "Unknown"
            self.scons_env['TAG']      = "Unknown"

        # liunote use for liu's builder sample ,now remove below
        ########################################################################
        # Setup and define dx tool
        ########################################################################
        #self.scons_env["DX"] = "python tools/dd_dsl_2.py"
        #self.scons_env["DX_COM"] = "$DX $SOURCES $TARGET " + str(MACHINE.arch)
        #self.scons_env["BUILDERS"]["Dx"] = Builder(action = "$DX_COM",
        #                                            src_suffix = ".dx",
        #                                            suffix = ".h")

        ########################################################################
        # Pre-processed builder
        ########################################################################
        #self.scons_env["CPPI"] = "python tools/dd_dsl.py"
        #self.scons_env["BUILDERS"]["CppI"] = Builder(action = "$CPPCOM",
        #                                            suffix = ".i")

        ########################################################################
        # Macro expanding pre-processed builder
        ########################################################################
        #self.scons_env["BUILDERS"]["MExp"] = Builder(
        #    action = Action("$CPPCOM", "[MEXP] $TARGET"),
        #    suffix = ".mi",
        #    source_scanner = SCons.Defaults.CScan
        #    )

        ########################################################################
        # Setup and define markup of templates
        ########################################################################
        #def build_from_template(target, source, env, for_signature = None):
        #    assert len(target) == 1
        #    assert len(source) == 1
        #    target = str(target[0])
        #    source = str(source[0])

        #    template = file(source).read()
        #    output = StringIO()
        #    result = markup(template, output, env["TEMPLATE_ENV"])
        #    assert result is True
        #    file(target, 'wb').write(output.getvalue())

        #self.scons_env["BUILDERS"]["Template"] = Builder(action = build_from_template,
        #                                                 src_suffix = ".template",
        #                                                 suffix = "")
        #self.scons_env["TEMPLATE_ENV"] = {}
    
    # liunote use for process gloabl config to CPPDEFINES
    def process_global_config(self):
        pass
        # xxx = get_int_arg(self, "XXX",default_xxx)
        # self.Append(CPPDEFINES =[("CONFIG_XXX", xxx)])

        # if not get_bool_arg(self, "ENABLE_DEBUG", True):
        #    self.scons_env.Append(CPPDEFINES = ["NDEBUG"])

        # get_arg , get_option_arg

    def set_scons_attr(self, target, attr, value):
        if isinstance(target, SCons.Node.NodeList):
            target = target[0]

        setattr(target.attributes, attr, value)


    def Package(self, package, buildname=None, duplicate = 0, **kargs):
        """Called to import a package. A package contains source files and
        a SConscript file. It may create multiple Libraries and Applications."""
        # Export env and args to the app SConstruct
	# liu1020 test
	# print buildname
	#def handle_arg(name, default):
        #    ret = kargs.get(name, default)
        #    if name in kargs: del kargs[name]
        #    return ret
	#for name in kargs:
	#  print name
	#  print "liu:Cross.py Package " + kargs[name]
	#  dir(kargs[name])
	#print self.args

        if buildname is None:
            buildname = package.replace(os.sep, "_")
        builddir = os.path.join(self.builddir, "object", buildname)

        args = copy.copy(self.args)
        args.update(kargs)
	#print self.name
	# dir(conf)
        if hasattr(conf, self.name) and hasattr(getattr(conf, self.name), buildname):
	    #print "liu:Cross.py Package hasattr "
	    #if kargs['serial_driver']=="s3c2410_uart":
	    #   print "kargs[s3c2410_uart]"
	    #if kargs['mtd_driver']=="s3c2410_nand":
	    #   print "kargs[s3c2410_nand]"
	    #for name in kargs:
            #   print name
	    #   print "liu:Cross.py Package " + kargs[name]
	    #print "update kargs"

            kargs.update(getattr(getattr(conf, self.name), buildname).dict)

        env = self
        args["buildname"] = buildname
        args["package"] = package
        args["system"] = self.system
        args["platform"] = self.machine.platform        # liunote use platform???

        # Call the apps SConstruct file to build it
	#os.path.join('cust', self.cust, package),
	#os.path.join(Dir("#/.").srcnode().abspath, 'cust', self.cust, package),
        paths = [
            package,
            os.path.join(Dir("#/.").srcnode().abspath, package),
            os.path.join(self.linux_dir, package),
	    # liunote for build rootfs( apps,rootfs,...)
	    os.path.join(self.project_dir, package),
        ]

        exports = ['env', 'args']

        for path in paths:
            sconscript_path = os.path.join(path, 'SConscript')
            if os.path.exists(sconscript_path):
                return SConscript(sconscript_path, build_dir=builddir,
                                  exports = exports, duplicate = duplicate)

        #raise UserError("Unable to find package: %s" % package)
        print UserError("Unable to find package: %s" % package)
        return (None, None, None)


    def BuildMenuLst(self, target, source, env):
        menulist_serial = """
        serial --unit=0 --stop=1 --speed=115200 --parity=no --word=8
        terminal --timeout=0 serial console
        """

        menulst_data = """
        default 0
        timeout = 0
        title = L4 NICTA::Iguana
        root (hd0,0)
        kernel=%s/image.elf
        """
        menulst = target[0]
        dir = env["ROOT"]

        menu = menulist_serial + menulst_data % dir

        open(str(menulst), "w").write(menu)


    ############################################################################
    # Methods called by SConscript  liunote scons 's method wraper
    ############################################################################
    class _extra:
        def __init__(self, data):
            self.data = data

    def _mogrify(self, kargs):
        for key in kargs:
            if isinstance(kargs[key], self._extra):
                kargs[key] = self.scons_env[key] + kargs[key].data
        return kargs

    def Extra(self, arg):
        return self._extra(arg)

    def AddPostAction(self, files, action, **kargs):
        return self.scons_env.AddPostAction(files, action, **self._mogrify(kargs))

    def Append(self, *args, **kargs):
        return self.scons_env.Append(*args, **self._mogrify(kargs))

    def AsmFile(self, *args, **kargs):
        return self.scons_env.AsmFile(*args, **self._mogrify(kargs))

    def CppFile(self, *args, **kargs):
        return self.scons_env.CppFile(*args, **self._mogrify(kargs))

    def CopyFile(self, target, source):
        return self.scons_env.InstallAs(target, source)

    def Command(self, target, source, action, **kargs):
        return self.scons_env.Command(target, source, action, **self._mogrify(kargs))

    def Depends(self, target, source):
        return self.scons_env.Depends(target, source)

    def CopyTree(self, target, source, test_func=None):
        ret = []
        for root, dirs, files in os.walk(Dir(source).srcnode().abspath, topdown=True):
            if "=id" in files:
                files.remove("=id")
            if ".arch-ids" in dirs:
                dirs.remove(".arch-ids")
            stripped_root = root[len(Dir(source).srcnode().abspath):]
            dest = Dir(target).abspath + stripped_root
            if files:
                for f in files:
                    src = root + os.sep + f
                    des = dest + os.sep + f
                    if not test_func or test_func(src):
                        ret += self.CopyFile(des, src)
            elif not dirs:
                ret += self.Command(Dir(dest), [], Mkdir(dest))
        return ret

    def CopySConsTree(self, target, sources):
        """
        Copy what SCons thinks should go into the source tree, not
        its actual contents.
        """
        def glob_nodes(target_dir, source_dir):
            target_nodes = []
            source_nodes = []

            for node in Dir(source_dir).all_children():
                if isinstance(node, SCons.Node.FS.Dir):
                    (t, s) = glob_nodes(target_dir.Dir(os.path.basename(node.path)), node)
                    target_nodes.append(t)
                    source_nodes.append(s)
                else:
                    target_nodes.append(target_dir.File(os.path.basename(node.path)))
                    source_nodes.append(node)

            return (flatten(target_nodes), flatten(source_nodes))

        if (type(sources) != type([])):
            sources = [sources]
        copies = []

        # Find all the files we need to copy.
        for source in sources:
            (target_nodes, source_nodes) = glob_nodes(Dir(target), Dir(source))
            for t, s in zip(target_nodes, source_nodes):
                copies.append(self.CopyFile(t, s))

        return copies
    
    #liunote ext2 tools
    def Ext2FS(self, size, dev):
        genext2fs = SConscript(os.path.join("tools/host", "genext2fs", "SConstruct"),
                               build_dir=os.path.join(self.builddir, "native", "tools", "genext2fs"),
                               duplicate=0)
        #ramdisk = self.builddir + os.sep + "ext2ramdisk"
	ramdisk = self.top_build_dir + os.sep + "images"+ os.sep + "ext2disk"
        cmd = self.Command(ramdisk, Dir(os.path.join(self.builddir,
                                                     "install")),
                           genext2fs[0].abspath +
                           " -b $EXT2FS_SIZE -d $SOURCE -f $EXT2FS_DEV $TARGET",
                           EXT2FS_SIZE=size, EXT2FS_DEV=dev)
        
	# liunote remove to top build dir 's images dir
      	if not os.path.exists(Dir(os.path.join(self.top_build_dir,"images")).abspath):
	    images_mkdir  = os.popen("mkdir %s " % Dir(os.path.join(self.top_build_dir,"images")).abspath)


        Depends(cmd, genext2fs)

        # always regenerate the ramdisk file
        AlwaysBuild(cmd)
        return cmd
    
    def MakeInitRamfs(self):

        """
	cpio_cmd = "find %s | cpio -o -H newc | gzip -9 > %s" % (Dir(os.path.join(self.builddir,"install")).abspath , \
	                      Dir(os.path.join(self.builddir,"images/ramdisk.gz")).abspath)
        # liunote is #path!
        #cpio_cmd = "find %s | cpio -o -H newc | gzip -9 > %s" % (os.path.join(self.builddir,"install"),
	#                      os.path.join(self.builddir,"../ramdisk.gz"))
        #print cpio_cmd
	#print self.builddir
	os.system(cpio_cmd)
	"""

        ramdisk =os.path.join(Dir(self.top_build_dir).abspath,"images/ramdisk.gz")  # liunote while Dir file then err:found where directory expected.
        # use xd path
	#ramdisk =os.path.join(self.builddir,"rootfs/ramdisk.gz")
	
	#print os.path.join(Dir(self.builddir).abspath,"install")

        #cpio_cmd = self.Command(ramdisk, Dir(os.path.join(self.builddir,
        #                                             "install")),
	#		      " find $SOURCE | cpio -o -H newc | gzip -9 >  $TARGET")

	cur_dir = os.getcwd()

        cpio_cmd = self.Command("./ramdisk.gz",[],
			      " cd $SOURCE_DIR ; find  . -print | cpio -o -H newc | gzip -9 >  $TARGET ; cd $CUR_DIR ",
	                       SOURCE_DIR = os.path.join(Dir(self.builddir).abspath,"install"), CUR_DIR = cur_dir)  # source dir must abs path find add -print ,nouse -9
         
	#print self.top_build_dir
	#images_mkdir = "mkdir %s " % Dir(self.top_build_dir)
	if not os.path.exists(Dir(os.path.join(self.top_build_dir,"images")).abspath):
	    images_mkdir  = os.popen("mkdir %s " % Dir(os.path.join(self.top_build_dir,"images")).abspath)

        #mv_cmd  = self.Command(ramdisk, []," mv $SOURCE_DIR $TARGET ", SOURCE_DIR = os.path.join(Dir(self.builddir).abspath,"install","ramdisk.gz") )
	#Depends(mv_cmd,cpio_cmd)
	#Depends(cpio_cmd, images_mkdir)
	
      
	AlwaysBuild(cpio_cmd)
	return cpio_cmd

   
    def MakeRamdisk(self,start_addr):
        #print str(start_addr)

        mkimage = SConscript(os.path.join("tools/host", "mkimage", "SConstruct"),
                               build_dir=os.path.join(self.builddir, "native", "tools", "mkimage"),
                               duplicate=0)
        
	ramdisk =os.path.join(Dir(self.top_build_dir).abspath,"images/u-tools.img")  # liunote while Dir file(not dir) then err:found where directory expected.
        """
        mkimage_cmd = self.Command(ramdisk,[],
                              mkimage[0].abspath +
			      " -A arm -O linux -T ramdisk -C none -a $START_ADDR -n \"ramdisk\" -d $SOURCE_RAMFS $TARGET ",
	                        START_ADDR="0x20800000", SOURCE_RAMFS = os.path.join(Dir(self.top_build_dir).abspath,"images/ext2ramdisk"))
        """

        mkimage_cmd = self.Command(ramdisk,[],
                             mkimage[0].abspath +
			      " -A arm -O linux -T ramdisk -C none -a $START_ADDR -n \"ramdisk\" -d $SOURCE_RAMFS $TARGET ",
	                        START_ADDR="0x20800000", SOURCE_RAMFS = os.path.join(Dir(self.builddir).abspath,"install","ramdisk.gz") )  #   os.path.join(Dir(self.top_build_dir).abspath,"images/ramdisk.gz")

        # mkimage -A arm -O linux -T ramdisk -C none -n ramdisk -d /tmp/images/initrd.img /home/liu/mk2rdisk.img
	# mkimage -A arm -O linux -T ramdisk -C none -a 0x20800000 -n "ramdisk" -d ramdisk.img ramdisk-uboot.img
        Depends(mkimage_cmd, mkimage)
	AlwaysBuild(mkimage_cmd)
	return mkimage_cmd


    def _build_objects(self, build_path, args, source_list):
        obj_dir = os.path.join(*([self.builddir] + build_path))
        objects = [self.scons_env.StaticObject(os.path.join(obj_dir, self.source_to_obj(src, '.o')), src, **args) for src in source_list]
        objects = Flatten(objects)
        return objects

    def _find_source(self, buildname, test_lib=False):
        """This function returns the globs used to automagically find
        source when the user doesn't supply it explicitly."""
        source = []
        #if buildname == "xxxx":
        #    buildname = os.path.join("xxx","xxx")
	"""
        dirs = ["src/",
                "src/%s/" % (self.kernel),
                "#arch/%s/%s/src/" % (self.machine.arch, buildname),
                "#arch/%s/%s/src/toolchain-%s/" % (self.machine.arch, buildname, self.toolchain.name),
                "#arch/%s/libs/%s/src/" % (self.machine.arch, buildname),
                "#cust/%s/arch/%s/%s/src/" % (self.cust, self.machine.arch, buildname),
                "#cust/%s/arch/%s/%s/src/toolchain-%s/" % (self.cust, self.machine.arch, buildname, self.toolchain.name),
                "#cust/%s/%s/cust/src/" % (self.cust, buildname),
                "#cust/%s/%s/cust/src/toolchain-%s/" % (self.cust, buildname, self.toolchain.name),
                "system/%s/src/" % self.system
                ]
        """
	dirs = ["src/"]
	
        #if xxx:
        #    dirs.append("xxx")
        for src_ext in self.src_exts:
            for dir_ in dirs:
                source.append(dir_ + "*." + src_ext)
        return source

    def add_public_headers(self, name, buildname, public_headers):
        if public_headers is None:
	    """
            public_headers = [("include/", "include/%(name)s/"),
                              ("include/%(kernel)s/", "include/%(name)s/"),
                              ("#arch/%(arch)s/%(name)s/include/", "include/%(name)s/arch/"),
                              ("#arch/%(arch)s/libs/%(name)s/include/", "include/%(name)s/arch/"),
                              ("#arch/%(arch)s/libs/%(name)s/v%(ver)s/include/", "include/%(name)s/arch/ver/"),
                              ("#cust/%(cust)s/%(name)s/include/", "include/%(name)s/cust/"),
                              ("#cust/%(cust)s/arch/%(arch)s/libs/%(name)s/include/", "include/%(name)s/arch/"),
                              ("#cust/%(cust)s/arch/%(arch)s/libs/%(name)s/v%(ver)s/include/", "include/%(name)s/arch/ver/"),
                              ("#cust/%(cust)s/arch/%(arch)s/%(name)s/include/", "include/%(name)s/arch/"),
                              ("system/%(system)s/include/", "include/%(name)s/system/"),
                              ("toolchain/%(toolchain)s/include/", "include/%(name)s/toolchain/")
                              ]
            """
	    # liunote will add public headers ???
            public_headers = [("include/", "include/%(name)s/")]

            #if self.test_libs and name in self.test_libs:
            #    public_headers.append(("test/", "include/%(name)s/"))

        # liunote remove some
        src_replacements = {#"arch" : self.machine.arch,
                            #"ver"  : self.machine.arch_version,
                            #"cust" : self.cust,
                            #"system" : self.system,
                            #"toolchain" : self.toolchain.name,
                            "name" : buildname,
                            }
        dst_replacements = {"name" : buildname}

        for src, dest in public_headers:
            headers = src_glob( (src + "*.h") % src_replacements)
            for header in headers:
                dir, header_file = os.path.split(header)
                destfile = os.path.join(self.builddir,dest % dst_replacements)
                destfile = os.path.join(destfile, header_file)
		#print header_file
		#print dir
		#print destfile
                self.headers += self.scons_env.InstallAs(destfile, header)

    def strip_suffix(self, filename):
        pos = filename.rfind(".")
        if pos == -1:
            return filename
        return filename[:pos]

    def source_to_obj(self, filename, suffix):
        base = self.strip_suffix(filename)
        if base.startswith('#'):
            base = base[1:]
        return base
        
        
    def CrossSharedLibrary(self, name, buildname=None, source=None, extra_source=None,
                     public_headers=None, extra_headers=None, dx_files=None,
                     di_files=None, reg_files=None, **kargs):
        """
        source is a list of 'globs' describing which files to compile.
        """
        if buildname is None:
            buildname = name

        # First step in building a library is installing all the
        # header files.
        self.add_public_headers(name, buildname, public_headers)

        kargs = self._mogrify(kargs)
        library_args = kargs

        # We want to make sure the source directory is in the include path
        # for private headers
        if "CPPPATH" not in library_args:
            library_args["CPPPATH"] = copy.copy(self.scons_env["CPPPATH"])
        library_args["CPPPATH"].append(Dir("src").abspath)

        if extra_headers is not None:
            self.headers += extra_headers

        if source is None:
            #test_lib = self.test_libs and name in self.test_libs
            source = self._find_source(name,test_lib=False)

        if extra_source is not None:
            source += extra_source
            
        source_list = Flatten([src_glob(glob) for glob in source])

        #objects = self._build_objects(["object", "shlibs_" + name], library_args, source_list)

        shlib_name = os.path.join(self.builddir, "shlib", "lib%s.so" % buildname)      # liunote build name not include lib
        #shlib = self.scons_env.SharedLibrary(shlib_name, objects, **library_args)
        shlib = self.scons_env.SharedLibrary(shlib_name, source_list, **library_args)

        self.shlibs += shlib

        return shlib


    def CrossStaticLibrary(self, name, buildname=None, source=None, extra_source=None,
                     public_headers=None, extra_headers=None, dx_files=None,
                     di_files=None, reg_files=None, **kargs):
        """
        source is a list of 'globs' describing which files to compile.
        """
        if buildname is None:
            buildname = name

        # First step in building a library is installing all the
        # header files.
        self.add_public_headers(name, buildname, public_headers)

        kargs = self._mogrify(kargs)
        library_args = kargs

        # We want to make sure the source directory is in the include path
        # for private headers
        if "CPPPATH" not in library_args:
            library_args["CPPPATH"] = copy.copy(self.scons_env["CPPPATH"])
        library_args["CPPPATH"].append(Dir("src").abspath)

        if extra_headers is not None:
            self.headers += extra_headers

        if source is None:
            #test_lib = self.test_libs and name in self.test_libs
            source = self._find_source(name,test_lib=False)

        if extra_source is not None:
            source += extra_source
            
        source_list = Flatten([src_glob(glob) for glob in source])

        objects = self._build_objects(["object", "libs_" + name], library_args, source_list)

        lib_name = os.path.join(self.builddir, "lib", "lib%s.a" % buildname)      # liunote build name not include lib
        lib = self.scons_env.StaticLibrary(lib_name, objects, **library_args)

        self.libs += lib

        return lib

    def CrossProgram(self, name, buildname=None, source=None, extra_source=None,
                     public_headers=None, relocatable=None, **kargs):
        """
        source is a list of 'globs' describing which files to compile.
        """
        if buildname is None:
            buildname = name

        self.add_public_headers(name, buildname, public_headers)

        kargs = self._mogrify(kargs)
        program_args = kargs
        program_args["CPPPATH"] = program_args.get("CPPPATH",
                                                   copy.copy(self.scons_env["CPPPATH"]))
        #print "liu cross program:"
	#print Dir("include")
        program_args["CPPPATH"].append(Dir("include"))

        depend_prefix = ""
        # liunote !!!
	if "LINKSCRIPTS" not in program_args or program_args["LINKSCRIPTS"] is None:
            # Is a default linker script provided?
            if (hasattr(self.machine, "link_script")):
                depend_prefix = "#"     # file comes from source dir, not builddir
                program_args["LINKSCRIPTS"] = self.machine.link_script

        libs = program_args.get("LIBS", [])
        program_args["LIBS"] = list(sets.Set(libs))

        if source is None:
            source = self._find_source(name)

        source_list = Flatten([src_glob(glob) for glob in source])

        # EVIL...  # liunote remove template
        #template_files = Flatten(src_glob("src/*.template"))
        #for file_name in template_files:
            # FIXME this is hacky
        #    template_env = kargs.get("TEMPLATE_ENV", self.scons_env["TEMPLATE_ENV"])
        #    template = self.scons_env.Template(file_name, TEMPLATE_ENV=template_env)
        #    self.scons_env.Depends(template, Value(template_env))
        #    source_list.append(str(template[0]))

        objects = self._build_objects([name, "object"], program_args, [src for src in source_list if not src.endswith(".template")])


        # Support for a list of pre-processed objects
        # eg. as needed by x86_64 for init32.cc
        if extra_source is not None:
            objects += extra_source

        # liunote remove CEG hack
        #if name == "xxx" and (self.machine.arch == "x86_64" or self.machine.arch == "ia32"):
        #    for obj in objects:
        #        self.Depends(obj, "include/xxx.h")
        #        self.Depends(obj, "include/xxx.h")

        # Generate targets for .i, pre-processed but not compiled files.
        # these aren't depended on by anything and target must be explicitly
        # defined on the command line to get them built.
        #[self.scons_env.CppI(x, **program_args)
        # for x in source_list if not x.endswith(".template")]


        # For GNU compilers, include our libgcc if needed # liunote remove
        #if self.toolchain.type == "gnu" and self.toolchain.dict["LIBGCC"] == "-lgcc":
        #    program_args['LIBS'] += ["gcc"]

        output = os.path.join(self.builddir, "bin", buildname)   # liunote now set to projects bin dir for output

        prog = self.scons_env.Program(output, objects, **program_args)

        self.set_scons_attr(prog, 'name', name)

        # FIXME: This should be part of the program scanner, libc might
        # be built after program  # liunote remove 
        #if self.scons_env.get("CRTOBJECT"):
        #    self.Depends(prog, self.scons_env["CRTOBJECT"])

        # liunote remove for linux app
        #if program_args.get("LINKSCRIPTS", None) is not None:
        #    self.Depends(prog, depend_prefix + program_args["LINKSCRIPTS"])

        # liunote nouse elf tool
        #self.Depends(prog, self.elfadorn)

        return prog

    def CrossCRTObject(self, source, **kargs):
        # Rule to build a C runtime object.
        output = os.path.join(self.builddir, "lib", "crt0.o")
        source_list = Flatten([src_glob(glob) for glob in source])
        if source_list == []:
            raise UserError, "No valid object in " + \
                  "CrossCRTObject from glob: %s" % source
        obj = self.scons_env.StaticObject(output, source_list, **kargs)
        self.scons_env["CRTOBJECT"] = obj

    def CrossTarball(self, target, source):
        """Create a compressed tarball of the source objects."""
        def make_tarball(target, source, env):
            tar = tarfile.open(str(target[0]), "w:gz");

            for entry in source:
                tar.add(entry.path,
                        entry.path.replace(env['TAR_PREFIX'], '', 1))
            tar.close()

        # Record the prefix of the source directory tree.  This will
        # be remove from files entering the tarball so that contants
        # will be relative to the to of the directory.
        #
        # HACK:  This only works if an environment is building just
        # one tarball.
        self.scons_env['TAR_PREFIX'] = os.path.dirname(Dir(source).path) + os.sep

        return self.scons_env.Command(target, source, Action(make_tarball, "[TAR ] $TARGET"))

    def CrossLicenceTool(self, target, source, sdk):
        """Run the Licence Tool over a directory in expand mode."""
        def expand_licence(target, source, env):
            for src in source:
                sdk_dir = os.path.abspath(str(src))
                cmd_str = "licence_tool --type=%s --mode=expand %s"
                print "Executing: %s" % (cmd_str % (["complete", "sdk"][sdk], sdk_dir))
                ret = os.system(cmd_str % (["complete", "sdk"][sdk], sdk_dir))

                if ret != 0:
                    raise UserError, "licence_tool returned error, perhaps it's not installed?"

        return self.scons_env.Command(target, source,
                                      Action(expand_licence, '[LIC ] $SOURCES'))

    def CrossSed(self, target, source, subs):
        """
        Copy source to target, running through sed with the given
        substitutions.  The subs is a dictionary of (sub, value)
        strings.

        Construction vars can be used in either part.
        """
        substr = ""

        # Because we can get paths as substitutions we need to repalce /'s with
        # \/'s so that sed can cope
        escape_fix = re.compile(r"/")
        for (k, v) in subs.iteritems():
            v = self.scons_env.subst(v)
            v = escape_fix.sub(r"\\/", v)
            substr += 's/%s/%s/; ' % (k, v)

        return self.Command(target, source,
            Action("sed -e '%s' <$SOURCES >$TARGET" % substr, '[SED ] $TARGET'))

    def CrossSedDelete(self, target, source, subs):
        """
        Copy source to target, running through sed with the given
        deletions.  The subs is a dictionary of strings.

        """
        substr = ""

        # Because we can get paths as substitutions we need to repalce /'s with
        # \/'s so that sed can cope
        escape_fix = re.compile(r"/")
        for v in subs:
            v = self.scons_env.subst(v)
            v = escape_fix.sub(r"\\/", v)
            substr += '/%s/d; ' % v

        return self.Command(target, source,
            Action("sed -e '%s' <$SOURCES >$TARGET" % substr, '[SED ] $TARGET'))

