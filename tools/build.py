#!/usr/bin/env python2.5
"""
This file contains the main implementation of the build system.
"""

if __name__ == "__main__":
    import sys
    print sys.path
    sys.path = ["tools"] + sys.path

    import SCons.Script
    import os
    #print os.getcwd()

    
    # We want to use -f, to force SCons to read tools/build.py as the
    # main SConscript. So we first make sure the user isn't trying
    # to use "-f" as well.
    for arg in sys.argv:
        if arg.startswith("-f"):
            print >> sys.stderr, "scons.py does not support the use of '-f'. " \
                  "Please use project=<projectname> syntax"
            sys.exit(1)

    # Append -f so that tools/build.py is used as top
    # level SConstruct
    sys.argv.append("-ftools%sscons.py" % os.path.sep)


    # Run the main script
    SCons.Script.main()

    # Just check we don't fall off the end.
    raise Exception("Must not get here")
